﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoManage;
internal class model {
}
// CommonExif 共有信息
internal class ExifData {
    DateTime TargetDateTime; // 确认的文件时间
    string TargetName;  // 确认的文件名

    DateTime CreateDate;       // 2019:02:0510:26:07
    DateTime ModifyDate;   // 2019:02:0510:26:07

    DateTime FileAccessDate;   // 2022:12:2511:49:51+08:00
    DateTime FileCreateDate;   // 2019:02:2115:19:58+08:00
    DateTime FileModifyDate;   // 2019:02:0518:26:08+08:00
    string Directory;         // 文件目录，E:/待整理
    string FileName;        // 文件名称：VID_20190205_182608.mp4
    string FileSize;        // 文件大小，eg:708 kB
    string FileType;         // 文件类型
    string FileTypeExtension;  // 以 . 开头的扩展名
    string MIMEType;         // image/jpeg

    // 图片专属属性
    DateTime DateTimeCreated; //2014:07:17 07:52:42
    DateTime DateTimeOriginal; //2014:07:17 07:52:42
    DateTime DigitalCreationDateTime; //2014:07:17 07:52:42
    int ImageHeight;            //3648
    int ImageWidth;              //5472
    DateTime MetadataDate;          //2014:09:19 12:44:54+08:00
    DateTime SubSecCreateDate;     //2014:07:17 07:52:42.00
    DateTime SubSecDateTimeOriginal;  //2014:07:17 07:52:42.00

    // 视频专属属性
    DateTime MediaCreateDate; // 2019:02:0510:26:07
    DateTime MediaModifyDate; // 2019:02:0510:26:07
    DateTime TrackCreateDate; // 2019:02:0510:26:07
    DateTime TrackModifyDate; // 2019:02:0510:26:07

    public static string[] AllTags = {
	// 图片专属tag
	"DateTimeCreated",         //2014:07:17 07:52:42
	"DateTimeOriginal",        //2014:07:17 07:52:42
	"DigitalCreationDateTime", //2014:07:17 07:52:42
	"ImageHeight",             //3648
	"ImageWidth",              //5472
	"MetadataDate",            //2014:09:19 12:44:54+08:00
	"SubSecCreateDate",        //2014:07:17 07:52:42gtime
	"SubSecDateTimeOriginal",  //2014:07:17 07:52:42.00
	// 视频专属tag
	"MediaCreateDate", //"2019:02:0510:26:07",
	"MediaModifyDate", //"2019:02:0510:26:07",
	"TrackCreateDate", //"2019:02:0510:26:07",
	"TrackModifyDate", //"2019:02:0510:26:07",
	// 公共tag
	"Directory",         //E:/待整理
	"CreateDate",        //2014:07:17 07:52:42
	"ModifyDate",        //2014:09:19 12:44:54
	"FileCreateDate",    //2014:09:19 12:44:51+08:00
	"FileModifyDate",    //2014:09:19 12:44:56+08:00
	"FileName",          //IMG_0825.jpg
	"FileSize",          //708 kB
	"FileType",          //JPEG
	"FileTypeExtension", //jpg
	"MIMEType",          //image/jpeg
};

}


// var ImageExtName = []string{
// 	".jpg", ".webp", ".jpeg", ".cr2", ".rw2", ".png",
// 	".bmp", ".tiff ", ".eps", ".gif", ".heif",
// 	".heic", ".ico", ".jfif", ".jif", ".netpbm",
// 	".pcx", ".psd", ".tga", ".tif", ".arw",
// 	".nef", ".orf", ".rwl", ".srw", "wemp"}
// var VideoExtName = []string{
// 	".mpeg", ".avi", ".mov", ".mp4", ".mpg", ".3gp",
// 	".mkv", ".m4v", ".flv", ".f4v", ".rmvb", ".rm",
// 	".vob", ".avci", ".3gpp", ".3g2", ".3gpp2",
// 	".webm", ".ts", ".wmv", ".asf",
// }

// addFileType("MP3", FILE_TYPE_MP3, "audio/mpeg", MtpConstants.FORMAT_MP3);
// addFileType("MPGA", FILE_TYPE_MP3, "audio/mpeg", MtpConstants.FORMAT_MP3);
// addFileType("M4A", FILE_TYPE_M4A, "audio/mp4", MtpConstants.FORMAT_MPEG);
// addFileType("WAV", FILE_TYPE_WAV, "audio/x-wav", MtpConstants.FORMAT_WAV);
// addFileType("AMR", FILE_TYPE_AMR, "audio/amr");
// addFileType("AWB", FILE_TYPE_AWB, "audio/amr-wb");
// if (isWMAEnabled()) {
// 	addFileType("WMA", FILE_TYPE_WMA, "audio/x-ms-wma", MtpConstants.FORMAT_WMA);
// }
// addFileType("OGG", FILE_TYPE_OGG, "audio/ogg", MtpConstants.FORMAT_OGG);
// addFileType("OGG", FILE_TYPE_OGG, "application/ogg", MtpConstants.FORMAT_OGG);
// addFileType("OGA", FILE_TYPE_OGG, "application/ogg", MtpConstants.FORMAT_OGG);
// addFileType("AAC", FILE_TYPE_AAC, "audio/aac", MtpConstants.FORMAT_AAC);
// addFileType("AAC", FILE_TYPE_AAC, "audio/aac-adts", MtpConstants.FORMAT_AAC);
// addFileType("MKA", FILE_TYPE_MKA, "audio/x-matroska");

// addFileType("MID", FILE_TYPE_MID, "audio/midi");
// addFileType("MIDI", FILE_TYPE_MID, "audio/midi");
// addFileType("XMF", FILE_TYPE_MID, "audio/midi");
// addFileType("RTTTL", FILE_TYPE_MID, "audio/midi");
// addFileType("SMF", FILE_TYPE_SMF, "audio/sp-midi");
// addFileType("IMY", FILE_TYPE_IMY, "audio/imelody");
// addFileType("RTX", FILE_TYPE_MID, "audio/midi");
// addFileType("OTA", FILE_TYPE_MID, "audio/midi");
// addFileType("MXMF", FILE_TYPE_MID, "audio/midi");

// addFileType("MPEG", FILE_TYPE_MP4, "video/mpeg", MtpConstants.FORMAT_MPEG);
// addFileType("MPG", FILE_TYPE_MP4, "video/mpeg", MtpConstants.FORMAT_MPEG);
// addFileType("MP4", FILE_TYPE_MP4, "video/mp4", MtpConstants.FORMAT_MPEG);
// addFileType("M4V", FILE_TYPE_M4V, "video/mp4", MtpConstants.FORMAT_MPEG);
// addFileType("3GP", FILE_TYPE_3GPP, "video/3gpp",  MtpConstants.FORMAT_3GP_CONTAINER);
// addFileType("3GPP", FILE_TYPE_3GPP, "video/3gpp", MtpConstants.FORMAT_3GP_CONTAINER);
// addFileType("3G2", FILE_TYPE_3GPP2, "video/3gpp2", MtpConstants.FORMAT_3GP_CONTAINER);
// addFileType("3GPP2", FILE_TYPE_3GPP2, "video/3gpp2", MtpConstants.FORMAT_3GP_CONTAINER);
// addFileType("MKV", FILE_TYPE_MKV, "video/x-matroska");
// addFileType("WEBM", FILE_TYPE_WEBM, "video/webm");
// addFileType("TS", FILE_TYPE_MP2TS, "video/mp2ts");
// addFileType("AVI", FILE_TYPE_AVI, "video/avi");
// 	addFileType("WMV", FILE_TYPE_WMV, "video/x-ms-wmv", MtpConstants.FORMAT_WMV);
// 	addFileType("ASF", FILE_TYPE_ASF, "video/x-ms-asf");

// addFileType("JPG", FILE_TYPE_JPEG, "image/jpeg", MtpConstants.FORMAT_EXIF_JPEG);
// addFileType("JPEG", FILE_TYPE_JPEG, "image/jpeg", MtpConstants.FORMAT_EXIF_JPEG);
// addFileType("GIF", FILE_TYPE_GIF, "image/gif", MtpConstants.FORMAT_GIF);
// addFileType("PNG", FILE_TYPE_PNG, "image/png", MtpConstants.FORMAT_PNG);
// addFileType("BMP", FILE_TYPE_BMP, "image/x-ms-bmp", MtpConstants.FORMAT_BMP);
// addFileType("WBMP", FILE_TYPE_WBMP, "image/vnd.wap.wbmp");
// addFileType("WEBP", FILE_TYPE_WEBP, "image/webp");

// addFileType("M3U", FILE_TYPE_M3U, "audio/x-mpegurl", MtpConstants.FORMAT_M3U_PLAYLIST);
// addFileType("M3U", FILE_TYPE_M3U, "application/x-mpegurl", MtpConstants.FORMAT_M3U_PLAYLIST);
// addFileType("PLS", FILE_TYPE_PLS, "audio/x-scpls", MtpConstants.FORMAT_PLS_PLAYLIST);
// addFileType("WPL", FILE_TYPE_WPL, "application/vnd.ms-wpl", MtpConstants.FORMAT_WPL_PLAYLIST);
// addFileType("M3U8", FILE_TYPE_HTTPLIVE, "application/vnd.apple.mpegurl");
// addFileType("M3U8", FILE_TYPE_HTTPLIVE, "audio/mpegurl");
// addFileType("M3U8", FILE_TYPE_HTTPLIVE, "audio/x-mpegurl");

// addFileType("FL", FILE_TYPE_FL, "application/x-android-drm-fl");

// addFileType("TXT", FILE_TYPE_TEXT, "text/plain", MtpConstants.FORMAT_TEXT);
// addFileType("HTM", FILE_TYPE_HTML, "text/html", MtpConstants.FORMAT_HTML);
// addFileType("HTML", FILE_TYPE_HTML, "text/html", MtpConstants.FORMAT_HTML);
// addFileType("PDF", FILE_TYPE_PDF, "application/pdf");
// addFileType("DOC", FILE_TYPE_MS_WORD, "application/msword", MtpConstants.FORMAT_MS_WORD_DOCUMENT);
// addFileType("XLS", FILE_TYPE_MS_EXCEL, "application/vnd.ms-excel", MtpConstants.FORMAT_MS_EXCEL_SPREADSHEET);
// addFileType("PPT", FILE_TYPE_MS_POWERPOINT, "application/mspowerpoint", MtpConstants.FORMAT_MS_POWERPOINT_PRESENTATION);
// addFileType("FLAC", FILE_TYPE_FLAC, "audio/flac", MtpConstants.FORMAT_FLAC);
// addFileType("ZIP", FILE_TYPE_ZIP, "application/zip");
// addFileType("MPG", FILE_TYPE_MP2PS, "video/mp2p");
// addFileType("MPEG", FILE_TYPE_MP2PS, "video/mp2p");

