﻿using System.Text.RegularExpressions;

namespace VCards.Helpers;

public static class VCardHelper
{
    public static string[] SplitCards(string contents)
    {
        var s = Regex.Split(contents, @"(BEGIN:VCARD.*?END:VCARD)",
            RegexOptions.Singleline );
        return s;
    }
}