﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace AddressBooks.Entities;

/// <summary>
/// 
///</summary>
[SugarTable("menu")]
public class Menu
{
    /// <summary>
    /// ID 
    ///</summary>
     [SugarColumn(ColumnName="menuId" ,IsPrimaryKey = true ,IsIdentity = true  )]
     public int MenuId { get; set; }
    /// <summary>
    /// 父ID 
    /// 默认值: 0
    ///</summary>
     [SugarColumn(ColumnName="parentId"    )]
     public int? ParentId { get; set; }
    /// <summary>
    /// 路由地址 
    ///</summary>
     [SugarColumn(ColumnName="path"    )]
     public string Path { get; set; }
    /// <summary>
    /// 路由名字 
    ///</summary>
     [SugarColumn(ColumnName="name"    )]
     public string Name { get; set; }
    /// <summary>
    /// 路由组件 
    ///</summary>
     [SugarColumn(ColumnName="component"    )]
     public string Component { get; set; }
    /// <summary>
    /// 重定向地址 
    ///</summary>
     [SugarColumn(ColumnName="redirect"    )]
     public string Redirect { get; set; }
    /// <summary>
    /// 菜单标题 
    ///</summary>
     [SugarColumn(ColumnName="title"    )]
     public string Title { get; set; }
    /// <summary>
    /// 顶级菜单图标 
    ///</summary>
     [SugarColumn(ColumnName="icon"    )]
     public string Icon { get; set; }
    /// <summary>
    /// 是否国际化 
    ///</summary>
     [SugarColumn(ColumnName="i18n"    )]
     public byte? I18n { get; set; }
    /// <summary>
    /// 是否显示 
    ///</summary>
     [SugarColumn(ColumnName="showlink"    )]
     public byte? Showlink { get; set; }
    /// <summary>
    /// 升序排序值 
    ///</summary>
     [SugarColumn(ColumnName="rank"    )]
     public byte? Rank { get; set; }
    /// <summary>
    /// 权限表 
    ///</summary>
     [SugarColumn(ColumnName="authority"    )]
     public string Authority { get; set; }
    /// <summary>
    /// 是否缓存 
    ///</summary>
     [SugarColumn(ColumnName="keepalive"    )]
     public byte? Keepalive { get; set; }
    /// <summary>
    /// 可打开最大数量 
    ///</summary>
     [SugarColumn(ColumnName="dynamiclevel"    )]
     public short? Dynamiclevel { get; set; }
    /// <summary>
    /// 刷新重定向 
    ///</summary>
     [SugarColumn(ColumnName="refreshredirect"    )]
     public string Refreshredirect { get; set; }
    /// <summary>
    /// 右侧是否有SVG图标 
    ///</summary>
     [SugarColumn(ColumnName="svg"    )]
     public byte? Svg { get; set; }
    /// <summary>
    /// SVG图标名称 
    ///</summary>
     [SugarColumn(ColumnName="svgname"    )]
     public string Svgname { get; set; }
}
