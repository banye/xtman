﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace AddressBooks.Entities;

/// <summary>
/// 微信关键字回复
///</summary>
[SugarTable("wx_keyreply")]
public class WxKeyreply
{
    /// <summary>
    /// 企业ID 
    ///</summary>
     [SugarColumn(ColumnName="CorpId" ,IsPrimaryKey = true   )]
     public string CorpId { get; set; }
    /// <summary>
    /// 应用ID 
    /// 默认值: 0
    ///</summary>
     [SugarColumn(ColumnName="AgentId" ,IsPrimaryKey = true   )]
     public int AgentId { get; set; }
    /// <summary>
    /// 关键字 
    ///</summary>
     [SugarColumn(ColumnName="Key" ,IsPrimaryKey = true   )]
     public string Key { get; set; }
    /// <summary>
    /// 回复内容 
    ///</summary>
     [SugarColumn(ColumnName="Reply"    )]
     public string Reply { get; set; }
    /// <summary>
    /// 回复类型：1为文字，2为图片，3为音频，4为视频，5为文章 
    /// 默认值: 0
    ///</summary>
     [SugarColumn(ColumnName="ReplyType"    )]
     public byte ReplyType { get; set; }
}
