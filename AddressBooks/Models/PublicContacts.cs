﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace AddressBooks.Entities
{
    /// <summary>
    /// 
    ///</summary>
    [SugarTable("pub_contacts")]
    public class PublicContacts
    {
        /// <summary>
        /// 姓名、单位的hash码 
        ///</summary>
         [SugarColumn(ColumnName="id" ,IsPrimaryKey = true   )]
         public uint Id { get; set; }
        /// <summary>
        /// 姓名 
        ///</summary>
         [SugarColumn(ColumnName="name"    )]
         public string Name { get; set; }
        /// <summary>
        /// 单位 
        ///</summary>
         [SugarColumn(ColumnName="org"    )]
         public string Org { get; set; }
        /// <summary>
        /// 单位ID 
        ///</summary>
         [SugarColumn(ColumnName="orgid"    )]
         public int? Orgid { get; set; }
        /// <summary>
        /// 职务 
        ///</summary>
         [SugarColumn(ColumnName="title"    )]
         public string Title { get; set; }
        /// <summary>
        /// 单位电话 
        ///</summary>
         [SugarColumn(ColumnName="work"    )]
         public string Work { get; set; }
        /// <summary>
        /// 手机号码（常用） 
        ///</summary>
         [SugarColumn(ColumnName="cell"    )]
         public string Cell { get; set; }
        /// <summary>
        /// 其他号码 
        ///</summary>
         [SugarColumn(ColumnName="other"    )]
         public string Other { get; set; }
        /// <summary>
        /// 排序 
        ///</summary>
         [SugarColumn(ColumnName="order"    )]
         public decimal? Order { get; set; }
        /// <summary>
        /// 是否在用 
        ///</summary>
         [SugarColumn(ColumnName="active"    )]
         public byte Active { get; set; }
        /// <summary>
        /// 更新时间 
        ///</summary>
         [SugarColumn(ColumnName="uptime"    )]
         public DateTime? Uptime { get; set; }
        /// <summary>
        /// 头像 
        ///</summary>
         [SugarColumn(ColumnName="avatar"    )]
         public string Avatar { get; set; }
    }
}
