﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using SqlSugar;
namespace AddressBooks.Entities
{
    /// <summary>
    /// 
    ///</summary>
    [SugarTable("org")]
    public class Org
    {
        /// <summary>
        ///  
        /// 默认值: 0
        ///</summary>
         [SugarColumn(ColumnName="id" ,IsPrimaryKey = true   )]
         public int Id { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "active")]
        public bool Active { get; set; }        /// <summary>
                                               ///  
                                               ///</summary>
        [SugarColumn(ColumnName = "enable")]
        public bool Enable { get; set; }        /// <summary>
                                               ///  
                                               ///</summary>
        [SugarColumn(ColumnName="old_id"    )]
         public short? OldId { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="parent_id"    )]
         public short? ParentId { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="count"    )]
         public int? Count { get; set; }
        /// <summary>
        ///  
        /// 默认值: 
        ///</summary>
         [SugarColumn(ColumnName="name"    )]
         public string Name { get; set; }
        /// <summary>
        ///  
        /// 默认值: 
        ///</summary>
         [SugarColumn(ColumnName="oldname"    )]
         public string Oldname { get; set; }
        /// <summary>
        ///  
        ///</summary>
         [SugarColumn(ColumnName="remark"    )]
         public string Remark { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SqlSugar.SugarColumn(IsIgnore = true)]
        public List<Org>? Children { get; set; }
    }
}
