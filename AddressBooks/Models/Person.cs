﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace AddressBooks.Entities;

/// <summary>
/// 
///</summary>
[SugarTable("person")]
public class Person
{
    /// <summary>
    ///  
    ///</summary>
     [SugarColumn(ColumnName="guid" ,IsPrimaryKey = true   )]
     public int Guid { get; set; }
    /// <summary>
    /// 户籍号 
    ///</summary>
     [SugarColumn(ColumnName="hjdid"    )]
     public int? Hjdid { get; set; }
    /// <summary>
    /// 城市 
    ///</summary>
     [SugarColumn(ColumnName="city"    )]
     public string City { get; set; }
    /// <summary>
    /// 县城 
    ///</summary>
     [SugarColumn(ColumnName="county"    )]
     public string County { get; set; }
    /// <summary>
    /// 乡镇 
    ///</summary>
     [SugarColumn(ColumnName="stret"    )]
     public string Stret { get; set; }
    /// <summary>
    /// 村 
    ///</summary>
     [SugarColumn(ColumnName="village"    )]
     public string Village { get; set; }
    /// <summary>
    /// 组 
    ///</summary>
     [SugarColumn(ColumnName="team"    )]
     public string Team { get; set; }
    /// <summary>
    /// 组代码 
    ///</summary>
     [SugarColumn(ColumnName="teamcode"    )]
     public string Teamcode { get; set; }
    /// <summary>
    /// 姓名 
    ///</summary>
     [SugarColumn(ColumnName="name"    )]
     public string Name { get; set; }
    /// <summary>
    /// 身份证号 
    ///</summary>
     [SugarColumn(ColumnName="idno"    )]
     public string Idno { get; set; }
    /// <summary>
    /// 性别 
    ///</summary>
     [SugarColumn(ColumnName="sex"    )]
     public string Sex { get; set; }
    /// <summary>
    /// 生日 
    ///</summary>
     [SugarColumn(ColumnName="birth"    )]
     public string Birth { get; set; }
    /// <summary>
    /// 配偶姓名 
    ///</summary>
     [SugarColumn(ColumnName="halfname"    )]
     public string Halfname { get; set; }
    /// <summary>
    /// 配偶证号 
    ///</summary>
     [SugarColumn(ColumnName="halfidno"    )]
     public string Halfidno { get; set; }
    /// <summary>
    /// 配偶生日 
    ///</summary>
     [SugarColumn(ColumnName="halfbirth"    )]
     public string Halfbirth { get; set; }
    /// <summary>
    ///  
    ///</summary>
     [SugarColumn(ColumnName="contact"    )]
     public string Contact { get; set; }
    /// <summary>
    ///  
    ///</summary>
     [SugarColumn(ColumnName="wis104"    )]
     public string Wis104 { get; set; }
    /// <summary>
    ///  
    ///</summary>
     [SugarColumn(ColumnName="wis1a6"    )]
     public string Wis1a6 { get; set; }
    /// <summary>
    ///  
    ///</summary>
     [SugarColumn(ColumnName="wis116"    )]
     public string Wis116 { get; set; }
    /// <summary>
    ///  
    ///</summary>
     [SugarColumn(ColumnName="wis1f3"    )]
     public string Wis1f3 { get; set; }
    /// <summary>
    ///  
    ///</summary>
     [SugarColumn(ColumnName="wissbdw"    )]
     public string Wissbdw { get; set; }
    /// <summary>
    ///  
    ///</summary>
     [SugarColumn(ColumnName="wissbsj"    )]
     public string Wissbsj { get; set; }
    /// <summary>
    ///  
    ///</summary>
     [SugarColumn(ColumnName="flow"    )]
     public string Flow { get; set; }
    /// <summary>
    ///  
    ///</summary>
     [SugarColumn(ColumnName="explain"    )]
     public string Explain { get; set; }
    /// <summary>
    ///  
    ///</summary>
     [SugarColumn(ColumnName="co_type"    )]
     public string CoType { get; set; }
    /// <summary>
    ///  
    ///</summary>
     [SugarColumn(ColumnName="co_date"    )]
     public string CoDate { get; set; }
    /// <summary>
    ///  
    ///</summary>
     [SugarColumn(ColumnName="ab_date"    )]
     public string AbDate { get; set; }
    /// <summary>
    ///  
    ///</summary>
     [SugarColumn(ColumnName="ab_reason"    )]
     public string AbReason { get; set; }
}
