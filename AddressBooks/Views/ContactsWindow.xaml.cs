﻿using AddressBooks.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AddressBooks.Views;

/// <summary>
/// ContactsWindow.xaml 的交互逻辑
/// </summary>
public partial class ContactsWindow : ContentControl
{
    public ContactsWindow()
    {
        InitializeComponent();
        ContactsViewModel vm = new();
        DataContext = vm;
    }

}