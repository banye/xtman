﻿using AddressBooks.ViewModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging.Messages;
using CommunityToolkit.Mvvm.Messaging;
using HandyControl.Controls;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AddressBooks.Views;

/// <summary>
/// ContactsManage.xaml 的交互逻辑
/// </summary>
public partial class ContactsRefresh
{
    public ContactsRefresh()
    {
        InitializeComponent();
    }

    private async void DataGrid_ScrollChanged(object sender, ScrollChangedEventArgs e)
    {
        if (e.VerticalChange == 0) return;
        StatusBarScrollViewMessage _data1 = new()
        {
            VerticalChange = e.VerticalChange,
            VerticalOffset = e.VerticalOffset,
            ViewportHeight = e.ViewportHeight,
            ExtentHeight = e.ExtentHeight
        };
        WeakReferenceMessenger.Default.Send(new ValueChangedMessage<StatusBarScrollViewMessage>(_data1), "token_class1");
        if (e.VerticalOffset + e.ViewportHeight >= e.ExtentHeight - 2 && !isGetData)
        {
            if (DataContext is ContactsRefreshViewModel crv)
            {
                isGetData = true;
                Debug.WriteLine("Get data");
                await crv.GetData();
                isGetData = false;
            }
        }
    }
    bool isGetData = false;
}
