﻿using HandyControl.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AddressBooks.Views;

/// <summary>
/// LeftContent.xaml 的交互逻辑
/// </summary>
public partial class PersonContent : UserControl
{
    public PersonContent() => InitializeComponent();

    private void SideMenuItem_Selected(object sender , RoutedEventArgs e)
    {
        SideMenuItem? sideMenuItem = sender as SideMenuItem;
        
        Growl.Success(sideMenuItem?.Header.ToString());
        
    }
}
