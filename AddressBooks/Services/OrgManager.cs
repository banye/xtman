﻿using SqlSugar;
using System;
using System.Collections.Generic;
using AddressBooks.Entities;
using System.Threading.Tasks;
using System.Runtime.Intrinsics.X86;
using System.Linq;
using System.Windows;
using System.Collections.ObjectModel;

namespace AddressBooks.Services;

public class OrgManager : Repository<Org>
{
    private static List<Org>? _orgs;
    //当前类已经继承了 Repository 增、删、查、改的方法

    //这里面写的代码不会给覆盖,如果要重新生成请删除 OrgManager.cs
    public async Task<List<Org>?> GetEmptyOrg()
    {
        var result = await Db.Queryable<Contacts>().Distinct()
            .Where(it => it.Orgid == 0 || it.Orgid == null || it.Orgid == 999999)
            .Select(it => new { it.Org }).ToListAsync();
        if (result != null)
        {
            List<Org> orgs = new();
            foreach (var item in result)
            {
                orgs.Add(new() { Name = item.Org, ParentId = 90 });
            }
            return orgs;
        }
        return null;
    }
    public async Task<List<Org>> GetTreeAsync(bool refresh = false)
    {
        if (refresh)
        {
            _orgs = await AsQueryable().ToTreeAsync(it => it.Children, it => it.ParentId, 0);
            if (_orgs != null)
                return _orgs;
        }
        if (_orgs != null)
            return _orgs;
        return await GetTreeAsync(true);
    }

    public List<Org> GetTree(bool refresh = false)
    {
        if (refresh)
        {
            _orgs = AsQueryable().ToTree(it => it.Children, it => it.ParentId, 0);
            if (_orgs != null)
                return _orgs;
        }
        if (_orgs != null)
            return _orgs;
        return GetTree(true);
    }

    public override  async Task<List<Org>> GetListAsync()
    {
        if (_orgs == null)
            await GetTreeAsync(true);
        return TreeToList(_orgs);
    }

    public override List<Org> GetList()
    {
        if (_orgs == null)
            GetTree(true);
        return TreeToList(_orgs);
    }

    /// <summary>
    /// 批量添加单位，并自动计算ID
    /// </summary>
    public async Task<bool> AddNewOrgsToDb(List<Org> list)
    {
        if (list.Count == 0) return false;
        // 从数据库读取本类最少编号
        var min = base.AsQueryable().Where(it => it.ParentId == list[0].ParentId)
            .OrderByDescending(it => it.Id).First();
        // 逐一编号
        int startNo = min == null ? Convert.ToInt32(list[0].ParentId) * 100 + 1 : min.Id + 1;
        foreach (var item in list)
            item.Id = startNo++;
        // 加入数据库
        return await base.InsertRangeAsync(list);
    }
    public async Task<bool> RecountOrgId(List<Org>? list)
    {
        if (list == null || list.Count == 0)
        {
            var orgs = GetTree();
            rebuildId(0, orgs);
            var newOrgs = TreeToList(orgs);
            Db.DbMaintenance.TruncateTable<Org>();
            return await base.InsertRangeAsync(newOrgs);
        }
        // ORM默认不支持修改主键，这种需求有2种方案
        // 删除当前记录，然后在添加一条新记录（因为主键都可以更新，说明没有外部引用，可以直接删掉在加）
        int i = 1;
        var newList = list.Select(it => new Org()
        {
            Id = Convert.ToInt32(it.ParentId * 100 + i++),
            Name = it.Name,
            ParentId = it.ParentId,
            OldId = it.OldId,
            Oldname = it.Oldname,
            Count = it.Count,
            Remark = it.Remark
        })
            .ToList();
        await Db.Deleteable<Org>(list).ExecuteCommandAsync();
        // FIXME:子单位如何更新
        return await base.InsertRangeAsync(newList);
    }
    /// <summary>
    /// 更新统计单位人数
    /// </summary>
    public static async Task<int> UpdateOrgCount()
    {
        return await Db.Updateable<Org>().SetColumns(
            it => new Org()
            {
                Count = SqlFunc.Subqueryable<Contacts>()
             .Where(s => s.Orgid == it.Id).Count(),
            })
            .Where(it => 1 == 1)
            .ExecuteCommandAsync();
    }

    private void rebuildId(int parentId, IEnumerable<Org>? list)
    {
        int i = 1;
        foreach (var item in list)
        {
            if (parentId != 0)
            {
                item.Id = parentId * 100 + i ++ ;
                item.ParentId = (short?)parentId;
            }
            if (item.Children != null)
            {
                rebuildId(item.Id, item.Children);
            }
            
        }
    }

    /**
     * 解析树形结构
     *
     * @param list
     * @return
     */
    private static List<Org> TreeToList(IEnumerable<Org> list)
    {
        List<Org> result = new();
        result.AddRange(list);
        foreach (Org entity in list)
        {
            if (entity.Children != null && entity.Children.Count > 0)
            {
                result.AddRange(TreeToList(entity.Children));
            }
        }
        return result;
    }

    #region 教学方法
    /// <summary>
    /// 仓储方法满足不了复杂业务需求，业务代码请在这里面定义方法
    /// </summary>
    public void Study()
    {

        /*********查询*********/

        var data1 = base.GetById(1);//根据ID查询
        var data2 = base.GetList();//查询所有
        var data3 = base.GetList(it => 1 == 1);  //根据条件查询  
                                                 //var data4 = base.GetSingle(it => 1 == 1);//根据条件查询一条,如果超过一条会报错

        var p = new PageModel() { PageIndex = 1, PageSize = 2 };// 分页查询
        var data5 = base.GetPageList(it => 1 == 1, p);
        Console.Write(p.TotalCount);//返回总数

        var data6 = base.GetPageList(it => 1 == 1, p, it => SqlFunc.GetRandom(), OrderByType.Asc);// 分页查询加排序
        Console.Write(p.TotalCount);//返回总数

        List<IConditionalModel> conModels = new List<IConditionalModel>(); //组装条件查询作为条件实现 分页查询加排序
        conModels.Add(new ConditionalModel() { FieldName = typeof(Org).GetProperties()[0].Name, ConditionalType = ConditionalType.Equal, FieldValue = "1" });//id=1
        var data7 = base.GetPageList(conModels, p, it => SqlFunc.GetRandom(), OrderByType.Asc);

        base.AsQueryable().Where(x => 1 == 1).ToList();//支持了转换成queryable,我们可以用queryable实现复杂功能



        /*********插入*********/
        var insertData = new Org() { };//测试参数
        var insertArray = new Org[] { insertData };
        base.Insert(insertData);//插入
        base.InsertRange(insertArray);//批量插入
        var id = base.InsertReturnIdentity(insertData);//插入返回自增列
        base.AsInsertable(insertData).ExecuteCommand();//我们可以转成 Insertable实现复杂插入



        /*********更新*********/
        var updateData = new Org() { };//测试参数
        var updateArray = new Org[] { updateData };//测试参数
        base.Update(updateData);//根据实体更新
        base.UpdateRange(updateArray);//批量更新
                                      //base.Update(it => new Org() { ClassName = "a", CreateTime = DateTime.Now }, it => it.id==1);// 只更新ClassName列和CreateTime列，其它列不更新，条件id=1
        base.AsUpdateable(updateData).ExecuteCommand();  //转成Updateable可以实现复杂的插入



        /*********删除*********/
        var deldata = new Org() { };//测试参数
        base.Delete(deldata);//根据实体删除
        base.DeleteById(1);//根据主键删除
        base.DeleteById(new int[] { 1, 2 });//根据主键数组删除
        base.Delete(it => 1 == 2);//根据条件删除
        base.AsDeleteable().Where(it => 1 == 2).ExecuteCommand();//转成Deleteable实现复杂的操作
    }
    #endregion


}