﻿using SqlSugar;
using System;
using System.Collections.Generic;
using AddressBooks.Entities;
using AddressBooks.ViewModel;
using System.Threading.Tasks;
using System.Diagnostics;

namespace AddressBooks.Services;

public class ContactsManager : Repository<Contacts>
{


    //当前类已经继承了 Repository 增、删、查、改的方法

    //这里面写的代码不会给覆盖,如果要重新生成请删除 ContactsManager.cs
    public List<Contacts> GetDefualtContacts(ref int count)
    {
        var p = new PageModel() { PageIndex = 1, PageSize = 15 };// 分页查询
        count = p.TotalCount;
        return base.GetPageList(it => 1 == 1, p);
    }

    public async Task<List<Contacts>?> GetContactsPageListAsync(Expressionable<Contacts> pars,List<string> orderBys, PageInfo pi)
    {
        RefAsync<int> tolCount = 0;
        var query = base.AsQueryable();
        if (orderBys != null)
        {
            foreach (var item in orderBys)
            {
                query.OrderBy(item.ToSqlFilter());//格式 id asc或者 id desc
            }
        }
            query.Where(pars.ToExpression());
        var result = await query.ToPageListAsync(pi.CurrentPageIndex, pi.Limit, tolCount);
        pi.TotalCount = tolCount;
        pi.TotalPages = pi.TotalCount / pi.Limit + 1;
        return result;
    }
    /// <summary>
    /// 更新通讯录里的单位代码
    /// </summary>
    public async static Task<int> UpdateOrgId()
    {
        return await  Db.Updateable<Contacts>().SetColumns(it => new Contacts()
        {
            Orgid = SqlFunc.Subqueryable<Org>()
             .Where(s => ( s.Name == it.Org && it.Org != "办公室") ||
             //将‘办公室’列入正常单位
             (it.Org == "办公室" && s.Name == it.Name))
             .Select(s => s.Id),
        }).Where(it => true)
             .ExecuteCommandAsync();
    }
    //private void TrimChar(ref List<Contacts> datas)
    //{
    //    char ts = '\'';
    //    foreach (var item in datas)
    //    {
    //        item.Name = item.Name.TrimStart(ts);
    //        item.Org = item.Org.TrimStart(ts);
    //        item.Title = item.Title.TrimStart(ts);
    //        item.Work = item.Work?.TrimStart(ts);
    //        item.Cell = item.Cell?.TrimStart(ts);
    //        item.Other = item.Other?.TrimStart(ts);
    //    }
    //}
    #region 教学方法
    /// <summary>
    /// 仓储方法满足不了复杂业务需求，业务代码请在这里面定义方法
    /// </summary>
    public void Study()
    {

        /*********查询*********/

        var data1 = base.GetById(1);//根据ID查询
        var data2 = base.GetList();//查询所有
        var data3 = base.GetList(it => 1 == 1);  //根据条件查询  
                                                 //var data4 = base.GetSingle(it => 1 == 1);//根据条件查询一条,如果超过一条会报错

        var p = new PageModel() { PageIndex = 1, PageSize = 2 };// 分页查询
        var data5 = base.GetPageList(it => 1 == 1, p);
        Console.Write(p.TotalCount);//返回总数

        var data6 = base.GetPageList(it => 1 == 1, p, it => SqlFunc.GetRandom(), OrderByType.Asc);// 分页查询加排序
        Console.Write(p.TotalCount);//返回总数

        List<IConditionalModel> conModels = new List<IConditionalModel>(); //组装条件查询作为条件实现 分页查询加排序
        conModels.Add(new ConditionalModel() { FieldName = typeof(Contacts).GetProperties()[0].Name, ConditionalType = ConditionalType.Equal, FieldValue = "1" });//id=1
        var data7 = base.GetPageList(conModels, p, it => SqlFunc.GetRandom(), OrderByType.Asc);

        base.AsQueryable().Where(x => 1 == 1).ToList();//支持了转换成queryable,我们可以用queryable实现复杂功能



        /*********插入*********/
        var insertData = new Contacts() { };//测试参数
        var insertArray = new Contacts[] { insertData };
        base.Insert(insertData);//插入
        base.InsertRange(insertArray);//批量插入
        var id = base.InsertReturnIdentity(insertData);//插入返回自增列
        base.AsInsertable(insertData).ExecuteCommand();//我们可以转成 Insertable实现复杂插入



        /*********更新*********/
        var updateData = new Contacts() { };//测试参数
        var updateArray = new Contacts[] { updateData };//测试参数
        base.Update(updateData);//根据实体更新
        base.UpdateRange(updateArray);//批量更新
        //base.Update(it => new Contacts() { ClassName = "a", CreateTime = DateTime.Now }, it => it.id==1);// 只更新ClassName列和CreateTime列，其它列不更新，条件id=1
        base.AsUpdateable(updateData).ExecuteCommand();  //转成Updateable可以实现复杂的插入



        /*********删除*********/
        var deldata = new Contacts() { };//测试参数
        base.Delete(deldata);//根据实体删除
        base.DeleteById(1);//根据主键删除
        base.DeleteById(new int[] { 1, 2 });//根据主键数组删除
        base.Delete(it => 1 == 2);//根据条件删除
        base.AsDeleteable().Where(it => 1 == 2).ExecuteCommand();//转成Deleteable实现复杂的操作
    }
    #endregion


}