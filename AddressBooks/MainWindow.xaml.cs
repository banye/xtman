﻿using AddressBooks.Views;
using AddressBooks.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AddressBooks;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow
{
    public MainWindow() => InitializeComponent();
    protected override void OnContentRendered(EventArgs e)
    {
        base.OnContentRendered(e);
        MainFrame.Content = new EmptyWindowContent();
        
    }
    private void SideMenuItem_Selected(object sender, RoutedEventArgs e)
    {
        if (MainFrame.Content?.GetType() != typeof(ContactsWindow))
        {
            MainFrame.Content = new ContactsWindow();
        }
    }
    private void SideMenuExchange_Selected(object sender, RoutedEventArgs e)
    {
        if (MainFrame.Content?.GetType() != typeof(ContactsExchange))
        {
            MainFrame.Content = new ContactsExchange();
        }
    }
    private void SideMenuRefresh_Selected(object sender, RoutedEventArgs e)
    {
        if (MainFrame.Content?.GetType() != typeof(ContactsRefresh))
        {
            MainFrame.Content = new ContactsRefresh();
        }
    }

    private void SideMenuOrg_Selected(object sender, RoutedEventArgs e)
    {
        if (MainFrame.Content?.GetType() != typeof(ContactsOrgWindow))
        {
            MainFrame.Content = new ContactsOrgWindow();
        }
    }

    private void SideMenuManage_Selected(object sender, RoutedEventArgs e)
    {
        if (MainFrame.Content?.GetType() != typeof(ContactsManage))
        {
            MainFrame.Content = new ContactsManage();
        }
    }
}