﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Messaging;
using CommunityToolkit.Mvvm.Messaging.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Interop;
using System.Xml.Linq;

namespace AddressBooks.ViewModel;

partial class MainViewModel : ObservableRecipient
{
    [ObservableProperty]
    private string statusText = string.Empty;

    public MainViewModel()
    {
        IsActive = true;
    }
    protected override void OnActivated()
    {
        //Register<>第一个类型一般是自己的类型,第2个是接收数据的类型
        //Register方法第1个参数一般是this,第2个参数是一个方法,可以获取接收到的值
        Messenger.Register<MainViewModel, string>(this, (r, message) =>
        {
            StatusText =  "  收到msg:" + message;
        });

        //Register<>第一个类型一般是自己的类型,第2个是接收数据的类型,第3个是token数据的类型
        //Register方法第1个参数一般是this,第2个参数是token,第3个参数是一个方法,可以获取接收到的值
        Messenger.Register<MainViewModel, ValueChangedMessage<string>, string>(this, "token_1", (r, message) =>
        {
            StatusText = "  收到msg:" + message.Value;
        });


        Messenger.Register<MainViewModel, ValueChangedMessage<StatusBarScrollViewMessage>, string>(this, "token_class", (r, msg) =>
        {
            StatusText = $"VerticalChange:{msg.Value.VerticalChange}  VerticalOffset:{msg.Value.VerticalOffset}  ViewportHeight:{msg.Value.ViewportHeight}  ExtentHeight:{msg.Value.ExtentHeight}";
        });

        Messenger.Register<MainViewModel, ValueChangedMessage<StatusBarScrollViewMessage>, string>(this, "token_class1", (r, msg) =>
        {
            StatusText = $"竖直变化:{msg.Value.VerticalChange}  竖直偏移:{msg.Value.VerticalOffset}  视口高度:{msg.Value.ViewportHeight}  扩展高度:{msg.Value.ExtentHeight}";
        });
        
        Messenger.Register<MainViewModel, StatusBarScrollViewMessage, string>(this, "token_Response", (r, message) =>
        {
            StatusText = "  收到msg:" + message.ToString();
            //Reply是答复 ,这样可以返回值 
            message.Reply("UserControlTopViewModel给你返回值");
        });
    }
}
/// <summary>
/// 必须继承RequestMessage  RequestMessage<string>代表返回数据的类型是string
/// </summary>
public class StatusBarScrollViewMessage: RequestMessage<string>
{
    public double VerticalChange ;
    public double VerticalOffset;
    public double ViewportHeight;
    public double ExtentHeight;
}