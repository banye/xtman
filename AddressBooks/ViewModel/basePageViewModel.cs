﻿using AddressBooks.Entities;
using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddressBooks.ViewModel;
public class PageInfo
{
    public int TotalCount { get; set; }
    public int TotalPages { get; set; }
    public int Limit { get; set; }
    public int CurrentPageIndex { get; set; }

}

public partial class BasePageViewModel<T>:ObservableObject
{
    [NotifyPropertyChangedFor(nameof(Pager))]
    [ObservableProperty]
    private List<T>? items;
    [ObservableProperty]
    private List<T>? selectedItems;
    [ObservableProperty]
    protected PageInfo pager;

}
