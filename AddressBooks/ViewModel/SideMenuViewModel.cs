﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HandyControl.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddressBooks.ViewModel;

partial class SideMenuViewModel : ObservableObject
{
    /// <summary>
    /// MVVM模式的可观测属性
    /// </summary>
    [ObservableProperty]
    private int _counter = 0;
    /// <summary>
    /// 供视图界面调用的Command定义
    /// </summary>
    [RelayCommand]
    private void OnCounterIncrement()
    {
        Counter++;
    }

    /// <summary>
    /// 供视图界面调用的Command定义
    /// </summary>
    [RelayCommand]
    private void Select(string header) => Growl.Success(header);
}
