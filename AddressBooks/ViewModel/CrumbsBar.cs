﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace AddressBooks.ViewModel;

interface ICrumbsBar
{
    Task CrumbsBarSearchAsync(string key);
    void CrumbsBarLeftButtonClick();
    void CrumbsBarRightButtonClick();
    void CrumbsBarDownButtonClick();
    Task CrumbsBarRefreshButtonClick();
}
