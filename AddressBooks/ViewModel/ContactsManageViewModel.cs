﻿using AddressBooks.Entities;
using AddressBooks.Services;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HandyControl.Collections;
using HandyControl.Controls;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace AddressBooks.ViewModel;

public partial class ContactsManageViewModel : ObservableObject
{
    readonly ContactsManager service = new();
    public ManualObservableCollection<ShowContact> DataList { get; set; } = new();
    /// <summary>
    /// 未知单位检测
    /// </summary>
    [RelayCommand]
    private async Task CheckErrOrgContactsAsync()
    {
        Growl.Info("数据获取中......");
        DataList.CanNotify = false;
        var res = await service.AsQueryable().Where(it => it.Orgid == 999999 || it.Orgid == null)
            .OrderBy(it => it.Org)
            .ToListAsync();
        res?.ForEach(r => DataList.Add(new ShowContact(r, "单位信息不正确")));
        DataList.CanNotify = true;
        Growl.Success("数据获取完成");
    }
    /// <summary>
    /// 清空数据 
    /// </summary>
    [RelayCommand]
    private void ClearData()
    {
        DataList.Clear();
    }
    /// <summary>
    /// 重复信息检测
    /// </summary>
    [RelayCommand]
    private async Task CheckReplyContactsAsync()
    {
        Growl.Info("数据获取中......");
        DataList.CanNotify = false;
        // SELECT * FROM contacts a WHERE (EXISTS (SELECT * FROM contacts WHERE (`name`=a.name && `cell` = a.cell && `id`!=a.id)))
        var res = await service.AsQueryable().Where(
             it => SqlFunc.Subqueryable<Contacts>().Where(s => s.Id != it.Id && s.Name == it.Name && s.Cell == it.Cell).Any())
             .OrderBy(it => it.Name)
             .ToListAsync();
        res?.ForEach(r => DataList.Add(new ShowContact(r, "疑似重复")));
        DataList.CanNotify = true;
        Growl.Success("数据获取完成");
    }
    [RelayCommand]
    private async Task DelSelectedItems(object sender)
    {
        if (sender is System.Collections.IList contacts)
        {
            //var Ids = contacts.Select(it => it.Id).ToList();
            DataList.CanNotify = false;
            uint[] Ids = new uint[contacts.Count];
            int i = 0;
            foreach (ShowContact item in contacts)
            {
                Debug.WriteLine(item.Name);
                Ids[i++] = item.Id;
                DataList.Remove(item);
            }
            //Growl.Success(string.Join(",",contacts));
            var res = await service.AsDeleteable().In(Ids).ExecuteCommandAsync();
            if (res > 0)
            {
                Growl.Success($"删除 {res} 条项目成功");
            }
            else
            {
                Growl.Info("删除项目失败,请刷新数据");
            }
            DataList.CanNotify = true;
        }
    }

    [RelayCommand]
    private async Task DelSelectedItemAsync(object sender)
    {
        DataList.CanNotify = false;
        if (sender is ShowContact sc)
        {
            var res = await service.DeleteByIdAsync(sc.Id);
            if (res)
            {
                DataList.Remove(sc);
                Growl.Success("删除项目成功");
            }
            else Growl.Info("删除项目失败");
        }
        DataList.CanNotify = true;
    }
}
public class ShowContact : Contacts
{
    //public bool IsSelected { get; set; } = false;
    public string Message { get; set; }

    public ShowContact(Contacts it, string str = "")
    {
        Id = it.Id;
        Name = it.Name;
        Org = it.Org;
        Title = it.Title;
        Work = it.Work;
        Cell = it.Cell;
        Order = it.Order;
        Other = it.Other;
        Orgid = it.Orgid;
        Avatar = it.Avatar;
        Uptime = it.Uptime;
        Message = str;
    }
    public ShowContact(PublicContacts it, string str = "")
    {
        Id = it.Id;
        Name = it.Name;
        Org = it.Org;
        Title = it.Title;
        Work = it.Work;
        Cell = it.Cell;
        Order = it.Order;
        Other = it.Other;
        Orgid = it.Orgid;
        Avatar = it.Avatar;
        Uptime = it.Uptime;
        Message = str;
    }
}
