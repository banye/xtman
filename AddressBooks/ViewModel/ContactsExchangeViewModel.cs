﻿using AddressBooks.Entities;
using AddressBooks.Services;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HandyControl.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using VCards.Types;

namespace AddressBooks.ViewModel;

public partial class ContactsExchangeViewModel : ObservableObject
{
    [ObservableProperty]
    private List<Contacts> dataList;
    //<Button Content = "整库导出EXCEL" Style="{StaticResource ButtonInfo}"/>
    [RelayCommand]
    private void ExportAllExcel()
    {
        Growl.Info("导出EXCEL");
    }
    //<Button Content = "整库导出VCARD" Style="{StaticResource ButtonInfo}"/>
    [RelayCommand]
    private void ExportAllVcard() { }
    //<Button Content = "EXCEL整库导入" Style="{StaticResource ButtonWarning}"/>
    [RelayCommand]
    private void RebuildFromExcel()
    {
        Growl.Ask("将删除原来的所有数据，是否继续？", isConfirmed =>
        {
            if (isConfirmed)
            {
                Growl.Warning("原数据删除中");
            }
            else
            {
                Growl.Success("数据保留，不进行下一步操作");
            }
            return true;
        });
    }
    //<Button Content = "VCARD整库导入" Style="{StaticResource ButtonWarning}"/>
    [RelayCommand]
    private void RebuildFromVcard()
    {
        Growl.Ask("将删除原来的所有数据，是否继续？", isConfirmed =>
        {
            if (isConfirmed)
            {
                Growl.Warning("原数据删除中");
            }
            else
            {
                Growl.Success("数据保留，不进行下一步操作");
            }
            return true;
        });
    }
    //<Button Content = "EXCEL导入" Style="{StaticResource ButtonWarning}"/>
    [RelayCommand]
    private void ImportFromExcel()
    {
        Growl.Ask("将删除原来的所有数据，是否继续？", isConfirmed =>
        {
            if (isConfirmed)
            {
                Growl.Warning("原数据删除中");
            }
            else
            {
                Growl.Success("数据保留，不进行下一步操作");
            }
            return true;
        });
    }
    //<Button Content = "VCARD导入" Style="{StaticResource ButtonWarning}"/>
    [RelayCommand]
    private async void ImportFromVcard()
    {
        //创建一个打开文件的对话框
        Microsoft.Win32.OpenFileDialog dialog = new()
        {
            Filter = "VCard Files|*.vcf",
            InitialDirectory = @"D:\",
        };
        //调用ShowDialog()方法显示该对话框，该方法的返回值代表用户是否点击了确定按钮
        if (dialog.ShowDialog().GetValueOrDefault())
        {
            var cards = VCards.Deserializer.Deserialize(dialog.FileName);

            Growl.Success("读取文件完成");
            var dl = await BigCardsToContactsAsync(cards);
             DataList = dl;
        }
        else
        {
            //Todo
            Growl.Info("操作取消");
            return;
        }
        //Growl.Ask("将删除原来的所有数据，是否继续？", isConfirmed =>
        //{
        //    if (isConfirmed)
        //    {
        //        Growl.Warning("原数据删除中");
        //    }
        //    else
        //    {
        //        Growl.Success("数据保留，不进行下一步操作");
        //    }
        //    return true;
        //});
    }
    private async Task<List<Contacts>> BigCardsToContactsAsync(IEnumerable<VCards.VCard> cards)
    {
        return await Task.Run(() => {
        var dl = from card in cards
                 select new Contacts()
                 {
                     Name = card.FormattedName ?? string.Empty,
                     Org = card.Organization ?? string.Empty,
                     Title = card.Title ?? string.Empty,
                     Cell = card.Telephones == null
                     ? string.Empty
                     : (from tel in card.Telephones
                        where tel.Type == TelephoneType.Cell
                        select tel.Number)
                            .FirstOrDefault()
                 };
            return dl.ToList();
        });
        
    }
    //private readonly OrgManager service = new ();
}
