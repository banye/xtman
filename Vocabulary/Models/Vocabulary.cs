﻿using System;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
namespace Vocabularys.Models;

/// <summary>
/// 单词表
///</summary>
[SugarTable("vocabulary")]
public class Vocabulary
{
    /// <summary>
    /// ID 
    ///</summary>
     [SugarColumn(ColumnName="id" ,IsPrimaryKey = true ,IsIdentity = true  )]
     public int Id { get; set; }
    /// <summary>
    /// 单词 
    ///</summary>
     [SugarColumn(ColumnName="word"    )]
     public string Word { get; set; }
    /// <summary>
    /// 翻译 
    ///</summary>
     [SugarColumn(ColumnName="translate"    )]
     public string Translate { get; set; }
    /// <summary>
    /// 词性 
    ///</summary>
     [SugarColumn(ColumnName="classes"    )]
     public string Classes { get; set; }
    /// <summary>
    /// 音标 
    ///</summary>
     [SugarColumn(ColumnName="phonetic"    )]
     public string Phonetic { get; set; }
    /// <summary>
    /// 备份 
    ///</summary>
     [SugarColumn(ColumnName="phonetic_bak"    )]
     public string PhoneticBak { get; set; }
    /// <summary>
    /// 级别 
    ///</summary>
     [SugarColumn(ColumnName="catelog"    )]
     public string Catelog { get; set; }
    /// <summary>
    /// 所在年级 
    ///</summary>
     [SugarColumn(ColumnName="grade"    )]
     public string Grade { get; set; }
    /// <summary>
    /// 所在单元 
    ///</summary>
     [SugarColumn(ColumnName="unit"    )]
     public string Unit { get; set; }
    /// <summary>
    /// 例句 
    ///</summary>
     [SugarColumn(ColumnName="sentence"    )]
     public string Sentence { get; set; }
    /// <summary>
    /// 导入的行 
    ///</summary>
     [SugarColumn(ColumnName="origin"    )]
     public string Origin { get; set; }
    /// <summary>
    /// 排序 
    ///</summary>
     [SugarColumn(ColumnName="orders"    )]
     public int? Orders { get; set; }
    /// <summary>
    /// 更新时间 
    ///</summary>
     [SugarColumn(ColumnName="UpdataTime"    )]
     public DateTime? UpdataTime { get; set; }
    /// <summary>
    ///  
    ///</summary>
     [SugarColumn(ColumnName="isImportant"    )]
     public bool? IsImportant { get; set; }
}
