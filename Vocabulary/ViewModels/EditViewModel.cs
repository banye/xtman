﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vocabularys.Models;
using Vocabularys.Services;

namespace Vocabularys.ViewModels;

partial class EditViewModel: ObservableObject {
    public EditViewModel() {
        Ids = services.GetIds().OrderBy(x=>x).ToList();
        currentEditWord = services.GetById(Ids[currentEditWordIndex]);
    }
    private readonly VocabularyManager services = new();
    private List<int> Ids;//= services.GetIds();
    private int currentEditWordIndex = 11;
    [ObservableProperty]
    private Vocabulary currentEditWord;
    [RelayCommand]
    private void NextEditWord()=> CurrentEditWord = services.GetById(Ids[++currentEditWordIndex]);
    [RelayCommand]
    private void PrevEditWord() => CurrentEditWord = services.GetById(Ids[--currentEditWordIndex]);
}
