﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vocabularys.Services;

class VocabularyManager : Repository<Models.Vocabulary> {
    public List<int> GetIds() {
        return AsQueryable().Select(x => x.Id).ToList();
    }
}
