﻿using HandyControl.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace 收入分析.Views;

/// <summary>
/// LoadItem.xaml 的交互逻辑
/// </summary>
public partial class LoadItem : UserControl
{
    public string Header
    {
        get { return (string)GetValue(HeaderProperty); }
        set { SetValue(HeaderProperty, value); }
    }
    public static readonly DependencyProperty HeaderProperty =
        DependencyProperty.Register("Header", typeof(string), typeof(LoadItem), new PropertyMetadata(null));

    public string FileName
    {
        get { return (string)GetValue(FileNameProperty); }
        set { SetValue(FileNameProperty, value); }
    }
    public static readonly DependencyProperty FileNameProperty =
    DependencyProperty.Register("FileName", typeof(string), typeof(LoadItem), new PropertyMetadata(null));

    public short TitleLine
    {
        get { return (short)GetValue(TitleLineProperty); }
        set { SetValue(TitleLineProperty, value); }
    }
    public static readonly DependencyProperty TitleLineProperty =
    DependencyProperty.Register("TitleLine", typeof(short), typeof(LoadItem), new PropertyMetadata((short)3));

    public short DataStartLine
    {
        get { return (short)GetValue(DataStartLineProperty); }
        set { SetValue(DataStartLineProperty, value); }
    }
    public static readonly DependencyProperty DataStartLineProperty =
    DependencyProperty.Register("DataStartLine", typeof(short), typeof(LoadItem), new PropertyMetadata((short)4));

    public static readonly DependencyProperty IsOnlyIncomeProperty =
        DependencyProperty.Register("IsOnlyIncome", typeof(bool), typeof(LoadItem), new PropertyMetadata(true));
    public bool IsOnlyIncome
    {
        get { return (bool)GetValue(IsOnlyIncomeProperty); }
        set { SetValue(IsOnlyIncomeProperty, value); }
    }

    public ICommand Command
    {
        get { return (ICommand)GetValue(CommandProperty); }
        set { SetValue(CommandProperty, value); }
    }
    public static readonly DependencyProperty CommandProperty =
        DependencyProperty.Register("Command", typeof(ICommand), typeof(LoadItem), new PropertyMetadata(default(ICommand)));

    public static readonly DependencyProperty CommandParameterProperty =
        DependencyProperty.Register("CommandParameter", typeof(object), typeof(LoadItem), new PropertyMetadata(null));

    public object CommandParameter
    {
        get { return (object)GetValue(CommandParameterProperty); }
        set { SetValue(CommandParameterProperty, value); }
    }


    public LoadItem()
    {
        InitializeComponent();
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
        //创建一个打开文件的对话框
        Microsoft.Win32.OpenFileDialog dialog = new()
        {
            Filter = "Excel Files|*.xlsx",
            //InitialDirectory = @"C:\Users\\Desktop",
        };
        //调用ShowDialog()方法显示该对话框，该方法的返回值代表用户是否点击了确定按钮
        if (dialog.ShowDialog().GetValueOrDefault())
        {
            FileName = dialog.FileName;
        }
        else
        {
            FileName = string.Empty;
        }
    }
}
