﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging.Messages;
using CommunityToolkit.Mvvm.Messaging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static 收入分析.Services.Util;
using System.Windows.Controls;
using 收入分析.Model;
using 收入分析.Services;

namespace 收入分析.ViewModel;

internal partial class ShowDataViewModel : ObservableObject {
    #region 属性
    [ObservableProperty]
    private List<string>? incomeDBList;
    [ObservableProperty]
    private string incomeDBSelect = string.Empty;
    [ObservableProperty]
    private List<string>? incomeStretList;
    [ObservableProperty]
    private string incomeStretSelect = string.Empty;
    [ObservableProperty]
    private List<string>? incomeVillageList;
    [ObservableProperty]
    private string incomeVillageSelect = string.Empty;
    [ObservableProperty]
    private List<收入信息2023>? showedIncomeList2023;
    [ObservableProperty]
    private List<收入信息2022>? showedIncomeList2022;
    [ObservableProperty]
    private List<Summary>? incomeSummaryList;
    [ObservableProperty]
    private List<IncomeRate>? incomeRateList = null;
    public ShowDataViewModel() {
        //IncomeDBList = SystemManager.GetDataBaseList();
    }
    #endregion

    #region 命令
    [RelayCommand]
    private async void Loaded() {
        if (string.IsNullOrEmpty(IncomeDBSelect)) {
            IncomeDBList = SystemManager.GM.DataBaseList;
        }
    }
    [RelayCommand]
    private void DbSelectChange() {
        Debug.WriteLine($"DbSelectChange :{IncomeDBSelect}");

        IncomeStretList =
            ShowDataManager.GetStretList(IncomeDBSelect!);
        ShowedIncomeList2023 = null;
        ShowedIncomeList2022 = null;

    }
    [RelayCommand]
    private void StretSelectChange() {
        Debug.WriteLine($"SelectChange :{IncomeStretSelect}");
        IncomeVillageList =
            ShowDataManager.GetVillageList(IncomeDBSelect, IncomeStretSelect);
    }
    [RelayCommand]
    private void VillageSelectChange() {
        Debug.WriteLine($"DbSelectChange :{IncomeVillageSelect}");
        switch (SystemManager.GetDataBaseType(IncomeDBSelect)) {
            case DataBaseType.全字段:
                ShowedIncomeList2022 = null;
                ShowedIncomeList2023 =
                      ShowDataManager.GetIncomeList2023(IncomeDBSelect, IncomeVillageSelect);
                break;
            case DataBaseType.系统收入2002:
                ShowedIncomeList2023 = null;
                ShowedIncomeList2022 = ShowDataManager.GetIncomeList2022(IncomeDBSelect, IncomeVillageSelect);
                break;
            default:
                break;
        }
    }
    [RelayCommand]
    private void IncomeAnalyVillageChange() {
        IncomeSummaryList = ShowDataManager.SummaryIncome(IncomeDBSelect, IncomeStretSelect, IncomeVillageSelect);
        IncomeRateList = null;
    }
    /// <summary>
    /// 计算收入汇总情况
    /// </summary>
    [RelayCommand]
    private void SummaryIncome() {
        IncomeRateList = null;
        IncomeSummaryList = ShowDataManager.SummaryIncome(IncomeDBSelect, IncomeStretSelect, IncomeVillageSelect);
    }
    /// <summary>
    /// 计算增幅情况
    /// </summary>
    [RelayCommand]
    private void RateIncome() {
        IncomeSummaryList = null;
    }
    #endregion

    #region 内部函数
    #endregion
}
