﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using CommunityToolkit.Mvvm.Messaging.Messages;
using Dm.filter.rw;
using HandyControl.Controls;
using MiniExcelLibs;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using 收入分析.Model;
using 收入分析.Services;
using Path = System.IO.Path;

namespace 收入分析.ViewModel;

internal partial class MainViewModel : ObservableRecipient
{
    #region 属性
    [ObservableProperty]
    private bool isLoadExcel = false;
    [ObservableProperty]
    //private IEnumerable<ExcelData>? excelDataList;
    private List<dynamic> excelDataList;
    [ObservableProperty]
    private string statusBarContent;
    [ObservableProperty]
    private string? systemDataPath2022;
    [ObservableProperty]
    private bool isOnlyIncome2022;
    [ObservableProperty]
    private string? importDataPath;
    [ObservableProperty]
    private IEnumerable<string>? incomeYearList;
    [ObservableProperty]
    private string? incomeYearSelect;
    [ObservableProperty]
    private List<string>? incomeStretList;
    [ObservableProperty]
    private string? incomeStretSelect;
    [ObservableProperty]
    private List<string>? incomeVillageList;
    [ObservableProperty]
    private string? incomeVillageSelect;
    [ObservableProperty]
    private string mainTabSelected;
    [ObservableProperty]
    private List<收入信息2023>? showedIncomeList2023;
    [ObservableProperty]
    private List<收入信息2022>? showedIncomeList2022;
    [ObservableProperty]
    private List<Summary> summaryIncomeList;
    #endregion

    #region 命令
    [RelayCommand]
    private void TabSelectionChanged(SelectionChangedEventArgs e)
    {
        if (e.OriginalSource is HandyControl.Controls.TabControl tabControl)
        {
            SystemManager.PostStatusBarMessage(MainTabSelected);
            switch (tabControl.SelectedValue)
            {
                case "加载数据":
                    break;
                case "收入分析":
                    //IncomeYearList = AllDataManager.GetTables();
                    //new() { "2022年度国家库", "2023年度国家库", "2022年度录入库", "2023年度录入库" };
                    break;
                case "数据分析":
                    break;
                case "配置":
                    break;
            }
        }
    }
    #endregion

    #region 内部函数
    public MainViewModel()
    {
        IsActive = true;
    }
    protected override void OnActivated()
    {

        //Register<>第一个类型一般是自己的类型,第2个是接收数据的类型,第3个是token数据的类型
        //Register方法第1个参数一般是this,第2个参数是token,第3个参数是一个方法,可以获取接收到的值
        Messenger.Register<MainViewModel, ValueChangedMessage<SystemManager.StatusBarMessage>, string>
            (this, "token_status_bar_msg", (r, msg) =>
        {
            StatusBarContent = msg.Value.Message;
        });

        //Messenger.Register<MainViewModel, StatusBarScrollViewMessage, string>(this, "token_Response", (r, message) =>
        //{
        //    StatusText = "  收到msg:" + message.ToString();
        //    //Reply是答复 ,这样可以返回值 
        //    message.Reply("UserControlTopViewModel给你返回值");
        //});
    }
    #endregion
}
