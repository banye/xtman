﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using 收入分析.Model;
using 收入分析.Services;

namespace 收入分析.ViewModel;

internal partial class AnalysisViewModel : ObservableObject {
    [ObservableProperty]
    private string? currentAnylysisTable;
    [ObservableProperty]
    private string? currentLastYearDataTable;
    // TODO: SQL是否常用、收藏、使用频次
    [ObservableProperty]
    private List<SQL> sQLs = AnalysisSQL.AnalysisSQLList;
    [ObservableProperty]
    private SQL selectedSQL;
    [ObservableProperty]
    private List<收入信息2023> analysisResult;

    public AnalysisViewModel() {
        currentAnylysisTable = SystemManager.GM.TablesInfoList
            .Where(x => x.IsDefault && x.DataDate.Year == 2023)
            .OrderByDescending(x => x.DataDate)
            .Select(x => x.Name).FirstOrDefault();
        currentLastYearDataTable = SystemManager.GM.TablesInfoList
            .Where(x => x.IsDefault && x.DataDate.Year == 2022)
            .OrderByDescending(x => x.DataDate)
            .Select(x => x.Name).FirstOrDefault();
    }
    [RelayCommand]
    private void AnalysisSelected() {
        if (!string.IsNullOrEmpty(CurrentAnylysisTable))
            AnalysisResult = FullDataManager.GetAnalysisResult(CurrentAnylysisTable, SelectedSQL.Exp);
    }
}
