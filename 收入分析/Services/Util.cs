﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using CommunityToolkit.Mvvm.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace 收入分析.Services;

internal static class Util
{
    internal static string ReplaceChar(object o)
    {
        return o.ToString()?.Replace("（", "")
             .Replace("）", "")
             .Replace("(", "")
             .Replace(")", "")
             .Replace("/", "")
        .Replace("_", "") ?? string.Empty
             ;
    }

}
