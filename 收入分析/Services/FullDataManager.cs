﻿using SqlSugar;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using 收入分析.Model;

namespace 收入分析.Services;

public class FullDataManager : Repository<全字段> {
    private FullDataManager() { }
    private static readonly Lazy<FullDataManager> db = new(() => new FullDataManager(), true);
    public static FullDataManager Instance { get { return db.Value; } }
    //当前类已经继承了 Repository 增、删、查、改的方法

    //这里面写的代码不会给覆盖,如果要重新生成请删除 JobManager.cs

    internal static async Task<bool> CleanTable(string table) {
        var tables = SystemManager.GM.DataBaseList;
        bool hasTable = tables.Contains(table);
        bool hasTemplateTable = tables.Contains("全字段");
        if (hasTable) {
            Db.DbMaintenance.TruncateTable(table);
        }
        else {
            try {
                if (hasTemplateTable) Db.Ado.ExecuteCommand($"CREATE TABLE {table} LIKE 全字段;");
                else {
                    Db.CodeFirst.As<全字段>(table).SetStringDefaultLength(20).InitTables<全字段>();
                }
                return true;
            }
            catch (Exception ex) {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        return true;
    }

    internal static List<收入信息2023> GetAnalysisResult(string db,Expression expression) {
        if (expression is Expression<Func<全字段, bool>> exp)
            return Db.Queryable<全字段>().AS(db).Where(exp).Select<收入信息2023>().ToList();
        return new();
    }

}