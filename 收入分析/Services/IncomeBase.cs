﻿using MiniExcelLibs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation.Peers;
using 收入分析.Model;

namespace 收入分析.Services;

internal class IncomeBase<T> : Repository<T> where T : class, new()
{
    internal static async Task<bool> CleanTableAsync(string table)
    {
        var tables = SystemManager.GM.DataBaseList;
        bool hasTable = tables.Contains(table);
        if (hasTable)
        {
            Db.DbMaintenance.TruncateTable(table);
        }
        else
        {
            try
            {
                Db.CodeFirst.As<T>(table).SetStringDefaultLength(20).InitTables<T>();
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }
        return true;
    }

    internal static async Task<int> Insert(string table, T t)
    {
        var i = await Db.Insertable(t).AS(table).ExecuteCommandAsync();
        return i;
    }

    internal Dictionary<int, string> GetExcelIncomeTitle(string filename, string titleline)
    {
        var keys = MiniExcel.Query(filename, startCell: titleline).Cast<IDictionary<string, object>>().First();
        var dic = new Dictionary<int, string>();
        var index = 0;
        foreach (var key in keys)
            dic[index++] = key.Value.ToString();
        return dic;
    }

}