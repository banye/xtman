﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using 收入分析.Model;

namespace 收入分析.Services;

internal class ShowDataManager : Repository<收入信息2023> {

    internal static List<string> GetStretList(DataBaseType t) => GetStretList(t.ToString());
    internal static List<string> GetStretList(string t) {

        return Db.Queryable<Address>()
          .AS(t)
          .Distinct()
          .Select(x => x.乡)
          .ToList();
    }

    internal static List<string> GetVillageList(DataBaseType t, string v) => GetVillageList(t.ToString(), v);
    internal static List<string> GetVillageList(string t, string v) =>
        Db.Queryable<Address>()
          .AS(t)
          .Where(x => x.乡 == v)
          .Distinct()
          .Select(x => x.村)
          .ToList();
    internal static List<收入信息2023> GetIncomeList(DataBaseType t, string v) => GetIncomeList2023(t.ToString(), v);
    internal static List<收入信息2023> GetIncomeList2023(string t, string village) =>
        Db.Queryable<收入信息2023>().AS(t).Where(x => x.村 == village && x.与户主关系 == "户主").Select<收入信息2023>().ToList();
    internal static List<收入信息2022> GetIncomeList2022(string t, string village) =>
        Db.Queryable<收入信息2022>().AS(t).Where(x => x.村 == village).Select<收入信息2022>().ToList();

    internal static List<Summary>? SummaryIncome(string table, string stret, string village) {
        ISugarQueryable<Summary>? sql = null;
        switch (SystemManager.GetDataBaseType(table)) {
            case DataBaseType.全字段:
                var sql1 = Db.Queryable<全字段>().AS(table)
                        .Where(x => x.与户主关系 == "户主")
                        .WhereIF(!string.IsNullOrEmpty(stret), x => x.乡 == stret)
                        .WhereIF(!string.IsNullOrEmpty(village), x => x.村 == village)
                        .GroupBy(x => new { x.村 })
                        .Select<Summary>(x => new() {
                            乡 = x.乡,
                            村 = x.村,
                            家庭人口数 = SqlFunc.Round(SqlFunc.AggregateSum(x.年度家庭人口数), 2),
                            纯收入 = SqlFunc.Round(SqlFunc.AggregateSum(x.纯收入_元) ?? 0, 2),
                            //人均纯收入 = SqlFunc.Round(SqlFunc.AggregateSum(x.人均纯收入_元) ?? 0, 2),
                        }).ToList();
                var sql2 = Db.Queryable<全字段>().AS(table)
                       .Where(x => x.与户主关系 == "户主" && x.户类型 == "脱贫户")
                        .WhereIF(!string.IsNullOrEmpty(stret), x => x.乡 == stret)
                        .WhereIF(!string.IsNullOrEmpty(village), x => x.村 == village)
                       .GroupBy(x => new { x.村 })
                       .Select<Summary>(x => new() {
                           乡 = x.乡,
                           村 = x.村,
                           脱贫人口数 = SqlFunc.Round(SqlFunc.AggregateSum(x.年度家庭人口数), 2),
                           脱贫人口纯收入 = SqlFunc.Round(SqlFunc.AggregateSum(x.纯收入_元) ?? 0, 2),
                           //脱贫人口人均纯收入 = SqlFunc.Round(SqlFunc.AggregateSum(x.人均纯收入_元) ?? 0, 2),
                       }).ToList();
                var sql3 = Db.Queryable<全字段>().AS(table)
                    .Where(x => x.与户主关系 == "户主" && x.风险是否已消除 != null)
                        .WhereIF(!string.IsNullOrEmpty(stret), x => x.乡 == stret)
                        .WhereIF(!string.IsNullOrEmpty(village), x => x.村 == village)
                    .GroupBy(x => new { x.村 })
                    .Select<Summary>(x => new() {
                        乡 = x.乡,
                        村 = x.村,
                        监测对象人口数 = SqlFunc.Round(SqlFunc.AggregateSum(x.年度家庭人口数), 2),
                        监测对象纯收入 = SqlFunc.Round(SqlFunc.AggregateSum(x.纯收入_元) ?? 0, 2),
                        //脱贫人口人均纯收入 = SqlFunc.Round(SqlFunc.AggregateSum(x.人均纯收入_元) ?? 0, 2),
                    }).ToList();
                var list = from s1 in sql1
                           join s2 in sql2 on s1.村 equals s2.村
                           join s3 in sql3 on s1.村 equals s3.村
                           select new Summary() {
                               乡 = s1.乡,
                               村 = s1.村,
                               家庭人口数 = s1.家庭人口数,
                               纯收入 = s1.纯收入,
                               脱贫人口数 = s2.脱贫人口数,
                               脱贫人口纯收入 = s2.脱贫人口纯收入,
                               监测对象人口数 = s3.监测对象人口数,
                               监测对象纯收入 = s3.监测对象纯收入,
                               //易地搬迁人口数 
                               //易地搬迁户纯收入 
                           };
                if (string.IsNullOrEmpty(village)) {
                    var summary = from l in list
                                  group l by l.乡 into nl
                                  select new Summary() {
                                      乡 = nl.Key,
                                      村 = "(全镇)",
                                      家庭人口数 = nl.Sum(m => m.家庭人口数),
                                      纯收入 = Math.Round(nl.Sum(m => m.纯收入), 2),
                                      脱贫人口数 = nl.Sum(m => m.脱贫人口数),
                                      脱贫人口纯收入 = nl.Sum(m => m.脱贫人口纯收入),
                                      监测对象人口数 = nl.Sum(m => m.监测对象人口数),
                                      监测对象纯收入 = nl.Sum(m => m.监测对象纯收入),
                                  };
                    list = summary.Concat(list);
                }
                var res = list.ToList();
                foreach (Summary s in res)
                    s.Calculate();
                return res;
            case DataBaseType.系统收入2002:
                var sql11 = Db.Queryable<收入信息2022>().AS(table)
                        .WhereIF(!string.IsNullOrEmpty(stret), x => x.乡 == stret)
                        .WhereIF(!string.IsNullOrEmpty(village), x => x.村 == village)
                        .GroupBy(x => new { x.乡, x.村 })
                        .Select<Summary>(x => new() {
                            乡 = x.乡,
                            村 = x.村,
                            家庭人口数 = SqlFunc.Round(SqlFunc.AggregateSum(x.年度家庭人口数), 2),
                            纯收入 = SqlFunc.Round(SqlFunc.AggregateSum(x.纯收入), 2),
                            //人均纯收入 = SqlFunc.Round(SqlFunc.AggregateSum(x.人均纯收入_元) ?? 0, 2),
                        }).ToList();
                var sql12 = Db.Queryable<收入信息2022>().AS(table)
                            //.Where(x => x.与户主关系 == "户主" && x.户类型 == "脱贫户")
                            .WhereIF(!string.IsNullOrEmpty(stret), x => x.乡 == stret)
                            .WhereIF(!string.IsNullOrEmpty(village), x => x.村 == village)
                        .Where(x => SqlFunc
                            .Subqueryable<全字段>()
                            .AS("全字段_2022") // FIXME: 放配置
                            .Where(o => x.户编号 == o.户编号 && o.户类型 == "脱贫户" && o.与户主关系 == "户主")
                            .Any())
                       .GroupBy(x => new { x.村 })
                       .Select<Summary>(x => new() {
                           乡 = x.乡,
                           村 = x.村,
                           脱贫人口数 = SqlFunc.Round(SqlFunc.AggregateSum(x.年度家庭人口数), 2),
                           脱贫人口纯收入 = SqlFunc.Round(SqlFunc.AggregateSum(x.纯收入), 2),
                           //脱贫人口人均纯收入 = SqlFunc.Round(SqlFunc.AggregateSum(x.人均纯收入_元) ?? 0, 2),
                       }).ToList();
                var sql13 = Db.Queryable<收入信息2022>().AS(table)
                        //.Where(x => x.与户主关系 == "户主" && x.风险是否已消除 != null)
                        .WhereIF(!string.IsNullOrEmpty(stret), x => x.乡 == stret)
                        .WhereIF(!string.IsNullOrEmpty(village), x => x.村 == village)
                        .Where(x => SqlFunc
                            .Subqueryable<全字段>()
                            .AS("全字段_2022") // FIXME: 放配置
                            .Where(o => x.户编号 == o.户编号 && o.风险是否已消除 != null && o.与户主关系 == "户主")
                            .Any())
                    .GroupBy(x => new { x.村 })
                    .Select<Summary>(x => new() {
                        乡 = x.乡,
                        村 = x.村,
                        监测对象人口数 = SqlFunc.Round(SqlFunc.AggregateSum(x.年度家庭人口数), 2),
                        监测对象纯收入 = SqlFunc.Round(SqlFunc.AggregateSum(x.纯收入), 2),
                        //脱贫人口人均纯收入 = SqlFunc.Round(SqlFunc.AggregateSum(x.人均纯收入_元) ?? 0, 2),
                    }).ToList();
                var list1 = from s1 in sql11
                            join s2 in sql12 on s1.村 equals s2.村
                            join s3 in sql13 on s1.村 equals s3.村
                            select new Summary() {
                                乡 = s1.乡,
                                村 = s1.村,
                                家庭人口数 = s1.家庭人口数,
                                纯收入 = s1.纯收入,
                                脱贫人口数 = s2.脱贫人口数,
                                脱贫人口纯收入 = s2.脱贫人口纯收入,
                                监测对象人口数 = s3.监测对象人口数,
                                监测对象纯收入 = s3.监测对象纯收入,
                                //易地搬迁人口数 
                                //易地搬迁户纯收入 
                            };
                if (string.IsNullOrEmpty(village)) {
                    var summary2022 = from l in list1
                                      group l by l.乡 into nl
                                      select new Summary() {
                                          乡 = nl.Key,
                                          村 = "(全镇)",
                                          家庭人口数 = nl.Sum(m => m.家庭人口数),
                                          纯收入 = Math.Round(nl.Sum(m => m.纯收入), 2),
                                          脱贫人口数 = nl.Sum(m => m.脱贫人口数),
                                          脱贫人口纯收入 = nl.Sum(m => m.脱贫人口纯收入),
                                          监测对象人口数 = nl.Sum(m => m.监测对象人口数),
                                          监测对象纯收入 = nl.Sum(m => m.监测对象纯收入),
                                      };
                    list1 = summary2022.Concat(list1);
                }
                var res2022 = list1.ToList();
                foreach (Summary s in res2022)
                    s.Calculate();
                return res2022;
            case DataBaseType.搬迁户信息:
                sql = Db.Queryable<收入信息2023>().AS(table)
                   .GroupBy(x => x.村).Select<Summary>(x => new() {
                       乡 = x.乡,
                       村 = x.村,
                       家庭人口数 = SqlFunc.Round(SqlFunc.AggregateSum(x.年度家庭人口数), 2),
                       纯收入 = SqlFunc.Round(SqlFunc.AggregateSum(x.纯收入_元) ?? 0, 2),
                       人均纯收入 = SqlFunc.Round(SqlFunc.AggregateSum(x.人均纯收入_元) ?? 0, 2),
                   });
                break;
            default:
                return null;
        }
        sql.OrderBy(x => new { x.乡, x.村 });
        return sql.ToList();
    }
}
