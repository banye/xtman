﻿using System;
using System.Collections.Generic;
using System.Linq;
using MiniExcelLibs.Attributes;
using SqlSugar;

namespace 收入分析.Model;

/// <summary>
/// 
///</summary>
//普通索引
[SugarIndex("{table}index_name", nameof(姓名), OrderByType.Asc)]
[SugarIndex("{table}index_关系", nameof(与户主关系), OrderByType.Asc)]
//唯一索引 (true表示唯一索引 或者叫 唯一约束)
//[SugarIndex("index_number", nameof(证件号码), OrderByType.Desc, true)]
public class 全字段
{
    /// <summary>
    ///  
    ///</summary>
    [ExcelColumnIndex(0)]
    public string 市 { get; set; }
    /// <summary>
    ///  
    ///</summary>
    [ExcelColumnIndex(1)]
    public string 县 { get; set; }
    /// <summary>
    ///  
    ///</summary>
    [ExcelColumnIndex(2)]
    public string 乡 { get; set; }
    /// <summary>
    ///  
    ///</summary>
    [ExcelColumnIndex(3)]
    public string 村 { get; set; }
    /// <summary>
    ///  
    /// 默认值: ''
    ///</summary>
    [ExcelColumnIndex(4)]
    public string 户编号 { get; set; }
    /// <summary>
    ///  
    /// 默认值: ''
    ///</summary>
    [ExcelColumnIndex(5)]
    public string 人口编号 { get; set; }
    /// <summary>
    ///  
    ///</summary>
    [ExcelColumnIndex(6)]
    public string 姓名 { get; set; }
    /// <summary>
    ///  
    /// 默认值: ''
    ///</summary>
    [SugarColumn(IsPrimaryKey = true)]
    [ExcelColumnIndex(7)]
    public string 证件号码 { get; set; }
    /// <summary>
    ///  
    ///</summary>
    [ExcelColumnIndex(8)]
    public string 与户主关系 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(9)]
    public double 当前家庭人口数 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(10)]
    public double 年度家庭人口数 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(11)]
    public string? 户类型 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(12)]
    public string? 监测对象类别 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(13)]
    public double? 工资性收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(14)]
    public double? 公益性岗位收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(15)]
    public double? 其他工资性收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(16)]
    public double? 财产性收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(17)]
    public double? 转移性收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(18)]
    public double? 最低生活保障金 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(19)]
    public double? 特困人员救助供养金 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(20)]
    public double? 养老金或离退休金 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(21)]
    public double? 计划生育金 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(22)]
    public double? 生态补偿金 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(23)]
    public double? 产业奖励 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(24)]
    public double? 就业奖励 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(25)]
    public double? 其他转移性收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(26)]
    public double? 生产经营性收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(27)]
    public double? 生产经营性支出 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(28)]
    public double? 生产经营性支出_合计 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(29)]
    public double? 专项用于减少生产经营性支出的补贴 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(30)]
    public double? 年收入_元 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(31)]
    public double? 纯收入_元 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(32)]
    public double? 人均纯收入_元 { get; set; }
    /// <summary>
    ///  
    ///</summary>
    [ExcelColumnIndex(33)]
    public string? 省 { get; set; }
    /// <summary>
    ///  
    ///</summary>
    [ExcelColumnIndex(34)]
    public string? 性别 { get; set; }
    /// <summary>
    ///  
    ///</summary>
    [ExcelColumnIndex(35)]
    public string? 证件类型 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(36)]
    public int? 出生日期 { get; set; }
    /// <summary>
    ///  
    ///</summary>
    [ExcelColumnIndex(37)]
    public string? 民族 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(38)]
    public string? 家庭成员联系电话 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(39)]
    public string? 文化程度 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(40)]
    public string? 在校生状况 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(41)]
    public string? 劳动技能 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(42)]
    public double? 务工时间_月 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(43)]
    public string? 健康状况 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(44)]
    public string? 政治面貌 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(45)]
    public string? 务工所在地 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(46)]
    public string? 务工企业名称 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(47)]
    public string? 享受低保政策情况 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(48)]
    public string? 是否参加城镇职工基本养老保险 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(49)]
    public string? 是否参加大病保险 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(50)]
    public string? 是否参加城乡居民基本医疗保险 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(51)]
    public string? 是否参加城乡居民基本养老保险 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(52)]
    public string? 义务教育阶段未上学原因 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(53)]
    public string? 是否会讲普通话 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(54)]
    public string? 是否享受人身意外保险补贴 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(55)]
    public string? 是否参加商业补充医疗保险 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(56)]
    public string? 是否国外务工 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(57)]
    public string? 是否接受医疗救助 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(58)]
    public string? 是否接受其他健康扶贫 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(59)]
    public string? 是否参加城镇职工基本医疗保险 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(60)]
    public string? 享受特困供养政策情况 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(61)]
    public string? 公益性岗位 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(62)]
    public string? 公益性岗位月数 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(63)]
    public string? 就业渠道_易地搬迁后扶使用 { get; set; }
    /// <summary>
    ///  
    /// 默认值: ''
    ///</summary>
    [ExcelColumnIndex(64)]
    public string? 首次进入系统时间 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(65)]
    public string? 新生儿年度 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(66)]
    public string? 婚入年度 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(67)]
    public string? 户籍迁入年月 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(68)]
    public string? 补录年度 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(69)]
    public string? 失联人口回归 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(70)]
    public string? 刑满释放 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(71)]
    public string? 分户年度 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(72)]
    public string? 残疾证办证年度 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(73)]
    public string? 大专或本科毕业生未就业原因 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(74)]
    public string? 是否事实无人抚养儿童 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(75)]
    public string? 整户无劳动能力兜底保障户 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(76)]
    public string? 到户产业帮扶类型 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(77)]
    public string? 是否解决安全饮用水 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(78)]
    public string? 是否有创业致富带头人带动 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(79)]
    public string? 是否有龙头企业带动 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(80)]
    public string? 主要燃料类型 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(81)]
    public string? 是否有卫生厕所 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(82)]
    public string? 入户道路是否硬化 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(83)]
    public string? 是否危房户 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(84)]
    public double? 住房面积 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(85)]
    public double? 与村主干路距离 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(86)]
    public string? 是否通广播电视 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(87)]
    public string? 是否通生活用电 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(88)]
    public string? 是否通生产用电 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(89)]
    public string? 是否加入农民专业合作组织 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(90)]
    public double? 水面面积 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(91)]
    public double? 牧草地面积 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(92)]
    public double? 林果面积_亩 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(93)]
    public double? 退耕还林面积_亩 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(94)]
    public double? 林地面积_亩 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(95)]
    public double? 耕地面积_亩 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(96)]
    public double? 其他财产性收入_旧指标 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    [ExcelColumnIndex(97)]
    public double? 资产收益扶贫分红收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(98)]
    public string? 风险是否已消除 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(99)]
    public string? 识别监测时间 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(100)]
    public int? 消除监测时间 { get; set; }
    /// <summary>
    ///  
    /// 默认值: ''
    ///</summary>
    [ExcelColumnIndex(101)]
    public string? 银行卡号 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(102)]
    public string? 开户银行 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(103)]
    public string? 户主证件号码 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(104)]
    public string? 户主姓名 { get; set; }
    /// <summary>
    ///  
    /// 默认值: ''
    ///</summary>
    [ExcelColumnIndex(105)]
    public string? 户联系电话 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(106)]
    public string? 是否军烈属 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(107)]
    public string? 脱贫年度 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(108)]
    public string? 识别标准 { get; set; }
    /// <summary>
    ///  
    /// 默认值: ''
    ///</summary>
    [ExcelColumnIndex(109)]
    public string? 组号 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(110)]
    public string? 实施开发式帮扶措施情况 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(111)]
    public string? 风险是否已消除0 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(112)]
    public string? 识别监测时间0 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(113)]
    public int? 消除监测时间0 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(114)]
    public string? 致贫返贫风险1 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(115)]
    public string? 致贫返贫风险2 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(116)]
    public string? 致贫返贫风险3 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(117)]
    public string? 致贫返贫风险4 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(118)]
    public string? 致贫返贫风险5 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(119)]
    public string? 因自然灾害子项 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(120)]
    public string? 其他备注 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(121)]
    public string? 是否义务教育阶段适龄儿童少年失学辍学 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(122)]
    public string? 是否住房损毁 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(123)]
    public string? 是否有家庭成员未参加城乡居民职工基本医疗保险 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(124)]
    public string? 是否饮水设施损毁 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(125)]
    public double? 工资性收入0 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(126)]
    public double? 财产性收入0 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(127)]
    public double? 转移性收入0 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(128)]
    public double? 生产经营性收入0 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(129)]
    public double? 生产经营性支出0 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(130)]
    public double? 家庭纯收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(131)]
    public double? 家庭人均纯收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(132)]
    public double? 理赔收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(133)]
    public double? 合规自付支出 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(134)]
    public double? 纳入监测对象的收入参考范围 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(135)]
    public double? 纳入监测对象的人均收入参考范围 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(136)]
    public string? 义务教育保障 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(137)]
    public string? 产业帮扶 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(138)]
    public string? 住房安全保障 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(139)]
    public string? 健康帮扶 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(140)]
    public string? 公益岗位帮扶 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(141)]
    public string? 公益性岗位帮扶其他备注 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(142)]
    public string? 就业帮扶 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(143)]
    public string? 搬迁 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(144)]
    public string? 生产生活条件改善 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(145)]
    public string? 教育帮扶 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(146)]
    public string? 社会帮扶 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(147)]
    public string? 金融帮扶 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(148)]
    public string? 饮水安全保障 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(149)]
    public string? 综合保障 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(150)]
    public string? 基础设施建设 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(151)]
    public string? 风险消除方式 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(152)]
    public string? 信息采集人 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(153)]
    public string? 信息采集人联系电话 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(154)]
    public string? 风险消除后是否义务教育阶段适龄儿童少年失学辍学 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(155)]
    public string? 风险消除后是否住房损毁 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(156)]
    public string? 风险消除后是否有家庭成员未参加城乡居民职工基本医疗保险 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(157)]
    public string? 风险消除后是否饮水设施损毁 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(158)]
    public string? 风险消除后工资性收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(159)]
    public string? 风险消除后财产性收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(160)]
    public string? 风险消除后转移性收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(161)]
    public string? 风险消除后生产经营性收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(162)]
    public string? 风险消除后生产经营性支出 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(163)]
    public string? 风险消除后家庭纯收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(164)]
    public string? 风险消除后家庭人均纯收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(165)]
    public string? 风险消除后理赔收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(166)]
    public string? 风险消除后合规自付支出 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(167)]
    public string? 义务教育阶段适龄儿童失学辍学问题解决时间 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(168)]
    public string? 义务教育阶段适龄儿童失学辍学问题解决时间修改次数 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(169)]
    public string? 住房安全问题解决时间 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(170)]
    public string? 住房安全问题解决时间修改次数 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(171)]
    public string? 家庭成员未全部参加城乡居民基本医疗保险解决时间 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(172)]
    public string? 家庭成员未全部参加城乡居民基本医疗保险问题解决时间修改次数 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(173)]
    public string? 收入未稳定超过4000元解决时间 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(174)]
    public string? 收入未稳定超过4000元解决时间修改次数 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(175)]
    public string? 饮水安全问题解决时间 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    [ExcelColumnIndex(176)]
    public string? 饮水安全问题解决时间修改次数 { get; set; }
}

