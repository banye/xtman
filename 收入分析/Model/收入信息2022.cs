﻿using System;
using System.Collections.Generic;
using System.Linq;
using MiniExcelLibs.Attributes;
using SqlSugar;
namespace 收入分析.Model;
/// <summary>
/// 
///</summary>
//普通索引
[SugarIndex("{table}index_name", nameof(姓名), OrderByType.Asc)]
//唯一索引 (true表示唯一索引 或者叫 唯一约束)
//[SugarIndex("index_number", nameof(证件号码), OrderByType.Desc, true)]
internal class 收入信息2022:Address
{

    public double 年度家庭人口数 { get; set; }
    public double 总收入 { get; set; }
    public double 人均纯收入 { get; set; }
    public double 纯收入 { get; set; }
    public double 工资性收入 { get; set; }
    public double 生产经营性收入 { get; set; }
    public double 生产经营性支出 { get; set; }
    public double 计划生育金 { get; set; }
    public double 低保金 { get; set; }
    public double 特困供养金 { get; set; }
    public double 养老保险金 { get; set; }
    public double 生态补偿金 { get; set; }
    public double 其他转移性收入 { get; set; }
    public double 资产收益扶贫分红收入 { get; set; }
    public double 其他财产性收入 { get; set; }
    internal void SetValue(string prop, object val)
    {
        if (val == null) return;
        var value = val.ToString();
        if (string.IsNullOrEmpty(value)) return;
        double i;
        switch (prop)
        {
            case "市州":
                市 = value;
                break;
            case "县市区":
                县 = value;
                break;
            case "乡镇":
                乡 = value;
                break;
            case "行政村":
                村 = value;
                break;
            case "户主姓名":
                姓名 = value;
                break;
            case "户主证件号码":
                证件号码 = value;
                break;
            case "家庭人口数":
                if (double.TryParse(value.ToString(), out i)) 年度家庭人口数 = i;
                break;
            case "户号":
                户编号 = value;
                break;
            case "工资性收入":
                if (double.TryParse(value.ToString(), out i)) 工资性收入 = i;
                break;
            case "资产收益扶贫分红收入":
                if (double.TryParse(value.ToString(), out i)) 资产收益扶贫分红收入 = i;

                break;
            case "低保金":
                if (double.TryParse(value.ToString(), out i)) 低保金 = i;

                break;
            case "特困供养金":
                if (double.TryParse(value.ToString(), out i)) 特困供养金 = i;

                break;
            case "养老保险金":
                if (double.TryParse(value.ToString(), out i)) 养老保险金 = i;

                break;
            case "计划生育金":
                if (double.TryParse(value.ToString(), out i)) 计划生育金 = i;

                break;
            case "生态补偿金":
                if (double.TryParse(value.ToString(), out i)) 生态补偿金 = i;

                break;
            case "其他转移性收入":
                if (double.TryParse(value.ToString(), out i)) 其他转移性收入 = i;

                break;
            case "生产经营性收入":
                if (double.TryParse(value.ToString(), out i)) 生产经营性收入 = i;

                break;
            case "生产经营性支出":
                if (double.TryParse(value.ToString(), out i)) 生产经营性支出 = i;

                break;
            case "总收入":
                if (double.TryParse(value.ToString(), out i)) 总收入 = i;

                break;
            case "纯收入":
                if (double.TryParse(value.ToString(), out i)) 纯收入 = i;
                break;
            case "人均纯收入":
                if (double.TryParse(value.ToString(), out i)) 人均纯收入= i;
                break;
        }
    }
}
