﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Xml.Linq;
namespace 收入分析.Model;

internal class AnalysisSQL {
    internal static readonly List<SQL> AnalysisSQLList = new() {
    new(){Id = "5_1_5",Details="脱贫人口人均纯收入低于1万元",Exp=Expressionable.Create<全字段>() //创建表达式
              .And(it => it.人均纯收入_元 < 10000 && it.与户主关系=="户主")
              .ToExpression()//注意 这一句 不能少
        },
        new(){Id = "5_1_6",Details="脱贫人口人均纯收入低于8000元",Exp=Expressionable.Create<全字段>() //创建表达式
              .And(it => it.人均纯收入_元 < 8000 && it.与户主关系=="户主")
              .ToExpression()
        },
    };
}
internal class SQL {
    public string Id { get; set; }
    public string Details { get; set; }
    public Expression Exp { get; set; }
};