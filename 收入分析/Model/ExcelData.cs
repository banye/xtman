﻿using MiniExcelLibs.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 收入分析.Model;

internal class ExcelData
{
    [ExcelColumnIndex(0)]
    public string 市州 { get; set; }
    [ExcelColumnIndex(1)]
    public string 县市区 { get; set; }
    [ExcelColumnIndex(2)]
    public string 乡镇 { get; set; }
    [ExcelColumnIndex(3)]
    public string 行政村 { get; set; }
    [ExcelColumnIndex(4)]
    public string 户号 { get; set; }
    [ExcelColumnIndex(5)]
    public string 户主姓名 { get; set; }
    [ExcelColumnIndex(6)]
    public string 户主证件号码 { get; set; }
    [ExcelColumnIndex(7)]
    public double 年度家庭人口数 { get; set; }
    [ExcelColumnIndex(8)]
    public double 公益性岗位收入 { get; set; }
    [ExcelColumnIndex(9)]
    public double 其他工资性收入 { get; set; }
    [ExcelColumnIndex(10)]
    public double 财产性收入 { get; set; }
    [ExcelColumnIndex(11)]
    public double 最低生活保障金 { get; set; }
    [ExcelColumnIndex(12)]
    public double 特困人员救助供养金 { get; set; }
    [ExcelColumnIndex(13)]
    public double 养老金或离退休金 { get; set; }
    [ExcelColumnIndex(14)]
    public double 计划生育金 { get; set; }
    [ExcelColumnIndex(15)]
    public double 生态补偿金 { get; set; }
    [ExcelColumnIndex(16)]
    public double 产业奖励 { get; set; }
    [ExcelColumnIndex(17)]
    public double 就业奖励 { get; set; }
    [ExcelColumnIndex(18)]
    public double 其他转移性收入 { get; set; }
    [ExcelColumnIndex(19)]
    public double 生产经营性收入 { get; set; }
    [ExcelColumnIndex(20)]
    public double 生产经营性支出 { get; set; }
    [ExcelColumnIndex(21)]
    public double 专项用于减少生产经营性支出的补贴 { get; set; }
    [ExcelColumnIndex(22)]
    public double 工资性收入 { get; set; }
    [ExcelColumnIndex(23)]
    public double 转移性收入 { get; set; }
    [ExcelColumnIndex(24)]
    public double 生产经营性支出合计 { get; set; }
    
    [ExcelColumnIndex(25)]
    public double 年度总纯收入2023 { get; set; }
    [ExcelColumnIndex(26)]
    public double 年度人均纯收入2023 { get; set; }
    [ExcelColumnIndex(27)]
    public double 年度总纯收入2022 { get; set; }
    [ExcelColumnIndex(28)]
    public double 年度人均纯收入2022 { get; set; }
    [ExcelColumnIndex(29)]
    public double 年度家庭人口2022 { get; set; }
    [ExcelColumnIndex(30)]
    public string 户类型 { get; set; }
    [ExcelColumnIndex(31)]
    public string 监测对象类别 { get; set; }
    [ExcelColumnIndex(32)]
    public string 识别监测时间 { get; set; }
    [ExcelColumnIndex(33)]
    public string 风险是否已消除 { get; set; }
    [ExcelColumnIndex(34)]
    public string 消除监测时间 { get; set; }
    [ExcelColumnIndex(35)]
    public string 易地搬迁户 { get; set; }
}
