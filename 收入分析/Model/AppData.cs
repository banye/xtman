﻿using Newtonsoft.Json.Linq;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 收入分析.Model;

/// <summary>
/// 应用信息
///</summary>
[SugarTable("sys_app_data")]//设置表名,如果不设置取类名为表名
internal class AppData
{
    public AppData()
    {
        
    }

    [SugarColumn(IsIdentity = true, IsPrimaryKey = true)]
    public int Id { get; set; }
    [SugarColumn(IsJson = true)]//必填
    public List<string> DBTypeList { get; set; } 
    // TODO: 导入时判断确定
    public string DefaultAnylysisTable { get; set; }  
    public string DefaultLastYearDataTable { get; set; }
    [SugarColumn(IsJson = true)]//必填
    public List<DataTableInfo> Tables { get; set; }
}
// [SugarTable("sys_table_info")]//设置表名,如果不设置取类名为表名
internal class DataTableInfo
{
    public int Id { get; set; }
    public string Name { get; set; }
    public DataBaseType Type { get; set; }
    public int RowNum { get; set; }
    public int ColumnNum { get; set; } 
    public DateTime ImportDate { get; set; }
    public DateTime DataDate { get; set; }
    public bool IsDefault { get; set; } 
    public DataTableInfo(DataBaseType type, DateTime dt)
    {
        Type = type;
        Name = $"{Type}_{dt:yyyy_MMdd}_{DateTime.Now:HHmm}";
        ImportDate = DateTime.Now;
        DataDate = dt;
        IsDefault = true;
    }
}
internal enum DataBaseType
{
    全字段,
    系统收入2002,
    采集收入,
    搬迁户信息,
}