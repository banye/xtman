﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 收入分析.Model;

public class Config
{
    //[Apps]
    //[DataBase]
    //[DataBase.Table]
    //[DataBase.Table.All]
    //Fields = 
    //[DataBase.Table.Income]
    //Fields = []
    public DataBase? DataBase{ get; set; }
    public ExcelTemplate[]? ExcelTemplate{ get; set; }
    public DataTable? DataTable { get; set; }
}
public class DataBase
{
    public string ConnString { get; set; } = "xczx.db";
}
public class DataTable
{
    public string DefaultAnylysisTable { get; set; } = "latest";
    public string DefaultLastYearDataTable { get; set; } = "latest";
}
public class ExcelTemplate
{
    public string Name { get; set; }
    public string[] Fields { get; set; }
}