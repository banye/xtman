﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 收入分析.Model;

internal class Summary
{
    public string 乡 { get; set; }
    public string 村 { get; set; }
    public double 家庭人口数 { get; set; }
    public double 纯收入 { get; set; }
    public double 人均纯收入 { get; set; }
    public double 脱贫人口数 { get; set; }
    public double 脱贫人口纯收入 { get; set; }
    public double 脱贫人口人均纯收入 { get; set; }
    public double 监测对象人口数 { get; set; }
    public double 监测对象纯收入 { get; set; }
    public double 监测对象人均纯收入 { get; set; }
    public double 易地搬迁人口数 { get; set; }
    public double 易地搬迁户纯收入 { get; set; }
    public double 易地搬迁户人均纯收入 { get; set; }
    internal void Calculate()
    {
        人均纯收入 = Math.Round(纯收入 / 家庭人口数, 2);
        脱贫人口人均纯收入 = Math.Round(脱贫人口纯收入 / 脱贫人口数, 2);
        监测对象人均纯收入 = Math.Round(监测对象纯收入 / 监测对象人口数, 2);
    }
}
