﻿using System;
using System.Collections.Generic;
using System.Linq;
using MiniExcelLibs.Attributes;
using SqlSugar;
namespace 收入分析.Model;
/// <summary>
/// 
///</summary>
//普通索引
[SugarIndex("{table}index_name", nameof(姓名), OrderByType.Asc)]
//唯一索引 (true表示唯一索引 或者叫 唯一约束)
//[SugarIndex("index_number", nameof(证件号码), OrderByType.Desc, true)]
internal class 收入信息2023:Address
{
    /// <summary>
    ///  
    ///</summary>
    public string 与户主关系 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    public double 年度家庭人口数 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    public string? 户类型 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    public string? 监测对象类别 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    public double? 工资性收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    public double? 公益性岗位收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    public double? 其他工资性收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    public double? 财产性收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    public double? 转移性收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    public double? 最低生活保障金 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    public double? 特困人员救助供养金 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    public double? 养老金或离退休金 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    public double? 计划生育金 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    public double? 生态补偿金 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    public double? 产业奖励 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    public double? 就业奖励 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    public double? 其他转移性收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    public double? 生产经营性收入 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    public double? 生产经营性支出 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    public double? 生产经营性支出_合计 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    public double? 专项用于减少生产经营性支出的补贴 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    public double? 年收入_元 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    public double? 纯收入_元 { get; set; }
    /// <summary>
    ///  
    /// 默认值: 0
    ///</summary>
    public double? 人均纯收入_元 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    public string? 风险是否已消除 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    public string? 识别监测时间 { get; set; }
    /// <summary>
    ///  
    /// 默认值: NULL
    ///</summary>
    public string? 消除监测时间 { get; set; }
    internal void SetValue(string prop, object val)
    {
        if (val == null) return;
        var value = val.ToString();
        if (string.IsNullOrEmpty(value)) return;
        double i;
        switch (prop)
        {
            case "县":
                县 = value;
                break;
            case "乡":
                乡 = value;
                break;
            case "村":
                村 = value;
                break;
            case "姓名":
                姓名 = value;
                break;
            case "证件号码":
                证件号码 = value;
                break;
            case "年度家庭人口数":
                if (double.TryParse(value.ToString(), out i)) 年度家庭人口数 = i;
                break;
            case "户类型":
                户类型 = value;
                break;
            case "与户主关系":
                与户主关系 = value;
                break;
            case "监测对象类别":
                监测对象类别 = value;
                break;
            case "工资性收入":
                if (double.TryParse(value.ToString(), out i)) 工资性收入 = i;

                break;
            case "公益性岗位收入":
                if (double.TryParse(value.ToString(), out i)) 公益性岗位收入 = i;

                break;
            case "其他工资性收入":
                if (double.TryParse(value.ToString(), out i)) 其他工资性收入 = i;

                break;
            case "财产性收入":
                if (double.TryParse(value.ToString(), out i)) 财产性收入 = i;

                break;
            case "转移性收入":
                if (double.TryParse(value.ToString(), out i)) 转移性收入 = i;

                break;
            case "最低生活保障金":
                if (double.TryParse(value.ToString(), out i)) 最低生活保障金 = i;

                break;
            case "特困人员救助供养金":
                if (double.TryParse(value.ToString(), out i)) 特困人员救助供养金 = i;

                break;
            case "养老金或离退休金":
                if (double.TryParse(value.ToString(), out i)) 养老金或离退休金 = i;

                break;
            case "计划生育金":
                if (double.TryParse(value.ToString(), out i)) 计划生育金 = i;

                break;
            case "生态补偿金":
                if (double.TryParse(value.ToString(), out i)) 生态补偿金 = i;

                break;
            case "产业奖励":
                if (double.TryParse(value.ToString(), out i)) 产业奖励 = i;

                break;
            case "就业奖励":
                if (double.TryParse(value.ToString(), out i)) 就业奖励 = i;

                break;
            case "其他转移性收入":
                if (double.TryParse(value.ToString(), out i)) 其他转移性收入 = i;

                break;
            case "生产经营性收入":
                if (double.TryParse(value.ToString(), out i)) 生产经营性收入 = i;

                break;
            case "生产经营性支出":
                if (double.TryParse(value.ToString(), out i)) 生产经营性支出 = i;

                break;
            case "生产经营性支出_合计":
                if (double.TryParse(value.ToString(), out i)) 生产经营性支出_合计 = i;

                break;
            case "专项用于减少生产经营性支出的补贴":
                if (double.TryParse(value.ToString(), out i)) 专项用于减少生产经营性支出的补贴 = i;

                break;
            case "年收入_元":
                if (double.TryParse(value.ToString(), out i)) 年收入_元 = i;

                break;
            case "纯收入_元":
                if (double.TryParse(value.ToString(), out i)) 纯收入_元 = i;

                break;
            case "人均纯收入_元":
                if (double.TryParse(value.ToString(), out i)) 人均纯收入_元 = i;

                break;
            case "风险是否已消除":
                风险是否已消除 = value;

                break;
            case "识别监测时间":
                识别监测时间 = value;

                break;
            case "消除监测时间":
                消除监测时间 = value;

                break;
        }
    }
}
