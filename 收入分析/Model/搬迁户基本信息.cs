﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 收入分析.Model;

[SugarIndex("{table}index_hulist", nameof(户编号), OrderByType.Asc)]
[SugarIndex("{table}index_name", nameof(姓名), OrderByType.Asc)]
internal class 搬迁户基本信息
{
    public string 省 { get; set; } = "湖南省";
    public string 市 { get; set; } = "永州市";
    public string 县 { get; set; } = "新田县";
    public string 乡 { get; set; } = string.Empty;
    public string 村 { get; set; } = string.Empty;
    public string? 户编号 { get; set; }
    public string? 姓名 { get; set; }
    [SugarColumn(IsPrimaryKey = true)]
    public string 证件号码 { get; set; }
    public bool 是否搬迁户 { get; set; }
}
