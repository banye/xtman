﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 收入分析.Model;

internal class IncomeRate {
    public string 乡 { get; set; }
    public string 村 { get; set; }
    public double 总体人口数2022年度 { get; set; }
    public double 人均纯收入2022年度 { get; set; }
    public double 总体人口数2023年度 { get; set; }
    public double 人均纯收入2023年度 { get; set; }
    public double 总体增幅 { get; set; }
    public double 脱贫人口数2022年度 { get; set; }
    public double 脱贫人口人均纯收入2022年度 { get; set; }
    public double 脱贫人口数2023年度 { get; set; }
    public double 脱贫人口人均纯收入2023年度 { get; set; }
    public double 脱贫人口增幅 { get; set; }
    public double 监测对象人口数2022年度 { get; set; }
    public double 监测对象人均纯收入2022年度 { get; set; }
    public double 监测对象人口数2023年度 { get; set; }
    public double 监测对象人均纯收入2023年度 { get; set; }
    public double 监测对象增幅 { get; set; }
    public double 易地搬迁人口数2022年度 { get; set; }
    public double 易地搬迁户人均纯收入2022年度 { get; set; }
    public double 易地搬迁人口数2023年度 { get; set; }
    public double 易地搬迁户人均纯收入2023年度 { get; set; }
    public double 易地搬迁户增幅 { get; set; }

}

